#!/usr/bin/env python
import ecogtopus
from ecogtopus.anatool.session_manager import AnalysisSetup

if __name__ == '__main__':
    if not ecogtopus.GUI_MODE:
        raise RuntimeError('ecogtopus library could not load in GUI mode')

    ast = AnalysisSetup()
    ast.configure_traits()
