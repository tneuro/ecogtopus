#!/usr/bin/env python
import ecogtopus
from ecogtopus.anatool.tonotopic import TonotopyAnalysisSetup

if __name__ == '__main__':
    if not ecogtopus.GUI_MODE:
        raise RuntimeError('ecogtopus library could not load in GUI mode')

    ast = TonotopyAnalysisSetup()
    ast.configure_traits()

