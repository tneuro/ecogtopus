import os.path as path
from setuptools import setup, find_packages
from glob import glob

 # with open('requirements.txt') as f:
#     reqs = f.readlines()
#     reqs = map(str.strip, reqs)
#     def _is_repo(s):
#         return s.lower().startswith('-e git')
#     def _del_soft_reqs(s):
#         if 'mayavi' in s.lower():
#             return False
#         if 'pyside' in s.lower():
#             return False
#         return not _is_repo(s)
#     reqs = filter(_del_soft_reqs, reqs)
#     links = filter(_is_repo, reqs)

setup(
    name='ecogtopus',
    version='0.1',
    packages=find_packages(exclude=['scripts']),
    scripts=glob(path.join('scripts', '*.py')),
    # install_requires=reqs,
    # dependency_links=links
    )
