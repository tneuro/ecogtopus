# Ideally there are three runtime modes:
# 1) GUI mode with a QtApp thread
#    In this mode, pyplot based plotting should not create canvases (or perhaps Agg canvas?) because plots will
#    become embedded as Qt panels with Qt canvas objects created by the traitsui_bridge modeul
# 2) Interactive IPython shell
#    Here pyplot should work as specified by the user's ipython parameters (eg "ipython --pylab=qt5")
# 3) Notebook mode
#    (This is perhaps identical to mode 2.) There may be two flavors of this. Headless mode, with ETS_TOOLKIT set
#    appropriately, should be compatible with the 2nd mode. On desktop/laptop mode it might be annoying to start
#    importing traitui. Maybe overwrite the ETS_TOOLKIT in this case?
#
# So for #3, perhaps over-ride ETS_TOOLKIT to None
# But how to distinguish #2 and #1 ??

from ecoglib.vis import plotters
from ecogdata.parallel.mproc import parallel_context
try:
    parallel_context.ctx = 'fork'
except:
    pass


GUI_MODE = True
if plotters.jupyter_runtime or plotters.headless:
    # Definitely can't run GUI MODE
    GUI_MODE = False
    from traits.etsconfig.api import ETSConfig
    ETSConfig.toolkit = 'null'
    # import os
    # os.environ['ETS_TOOLKIT'] = ''
if GUI_MODE:
    import matplotlib
    matplotlib.use('qt5agg')
    # matplotlib.use('Agg')
    from ecoglib.vis.plot_util import subplots
    from matplotlib.figure import Figure
    # pretty hacky but ... now can assume that no figures are created with canvases
    plotters.plt.figure = Figure
    plotters.plt.subplots = subplots

