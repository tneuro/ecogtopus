import gc
from glob import glob
import os
import numpy as np
from traits.api import HasTraits, Str, List, Button, Float, Enum, Bool, Tuple, Code, on_trait_change
from traitsui.api import View, Item, SetEditor, VGroup, HGroup, spring, Handler, EnumEditor, Group

from ecogdata.expconfig import available_sessions, session_conf, find_conf, uniform_bunch_case
from ecogdata.filt.time import lowpass_envelope
from ecogdata.devices.data_util import load_experiment_auto, join_datasets, mux_headstages, active_headstages, \
    find_loadable_files
from ecogdata.devices.current_tools import convert_amps_to_volts
from ecoglib.signal_testing import safe_avg_power, bad_channel_mask
try:
    from fast_scroller.loading import VisLauncher
    from fast_scroller.filtering import Filter, ButterMenu
    _SCROLLER = True
except ImportError:
    _SCROLLER = False

from .base import Analysis, Error
from .modules import ChannelAnalysis, AverageAnalysis, FrequencyAnalysis, ArrayMaps, Spectrogram, StimDecoding
from .filters_module import Filters


_all_groups = set([s.split('/')[0] for s in available_sessions()])
_all_groups = sorted(list(_all_groups))


def create_scroll_launcher(manager: 'AnalysisSetup'):
    session = manager.session
    if len(manager.proc_runs) != 1:
        raise ValueError('Can only scroll a single recording for now')
    recording = manager.proc_runs[0]
    recording = recording.split(',')[0]
    # Form the FileData instance
    sconf = uniform_bunch_case(session_conf(session))
    def _get_recording_key(k):
        if k in sconf[recording]:
            return sconf[recording][k]
        else:
            return sconf.session[k]
    electrode = _get_recording_key('electrode')
    headstage = _get_recording_key('headstage')
    if headstage in active_headstages:
        headstage = 'active'
    elif headstage == 'oephys':
        headstage = 'intan (oephys)'
    elif headstage == 'intan':
        headstage = 'intan (rhd)'
    fpath = find_loadable_files(session, recording, downsampled=True, create_downsampled=True)
    downsampled = True
    if fpath is None:
        fpath = find_loadable_files(session, recording, downsampled=False)
        downsampled = False
    if fpath is None:
        raise ValueError('There is no loadable data for {}-{}'.format(session, recording))
    vis = VisLauncher(headstage=headstage)
    if headstage == 'intan (oephys)':
        if downsampled:
            scale = 1e-6
        else:
            scale = 1.98e-7
            # need to find any continuous file
            fpath = glob(os.path.join(fpath, '*.continuous'))[0]
        vis.file_data.file = fpath
        vis.file_data.y_scale = scale
        if vis.file_data.can_convert:
            vis.file_data.do_convert = True
    elif headstage == 'active':
        vis.file_data.file = fpath
        daq = _get_recording_key('daq')
        vis.file_data.daq = daq
        vis.file_data.headstage = _get_recording_key('headstage')
        # channel map logic is handled by the file manager
        vis.file_data.electrode = electrode
        electrode = 'active'
        # filedata = ActiveArrayFileData(file=fpath, electrode=electrode, daq=daq)
    else:
        vis.file_data.file = fpath
    vis.chan_map = electrode

    # Now do filter setup
    f_lo, f_hi = manager.proc_bandpass
    if f_lo > 0:
        hp_butterworth = ButterMenu(lo=f_lo, filtfilt=True)
        hp_filter = Filter(filt_type='butterworth', filt_menu=hp_butterworth)
        vis.filters.filters.append(hp_filter)
    if f_hi > 0:
        lp_butterworth = ButterMenu(hi=f_hi, filtfilt=True)
        lp_filter = Filter(filt_type='butterworth', filt_menu=lp_butterworth)
        vis.filters.filters.append(lp_filter)

    return vis


class EmptyProcSet(Exception):
    pass


class SessionHandler(Handler):
    runs = List(Str)
    available_runs = List(Str)
    available_sessions = List(Str)

    def object_group_changed(self, info):
        info.object.proc_runs = list()
        sessions = available_sessions(group=info.object.group)
        self.available_sessions = [s.split('/')[1] for s in sessions]
        info.object.session_name = self.available_sessions[0]

    def object_session_name_changed(self, info):
        info.object.proc_runs = list()
        s = info.object.session_name
        g = info.object.group
        if not s or not g:
            return
        session = '/'.join([g, s])
        sconf = session_conf(session)

        # get the session config sections names that do not have reserved words
        session_items = [s for s in sorted(sconf.sections) if s not in ('session', 'tonotopy')]
        self.available_runs = session_items

    def object_proc_runs_changed(self, info):
        self.available_runs = sorted(self.available_runs)


class AnalysisSetup(HasTraits):
    """
    This object organizes sessions and their recordings, and
    launches Analysis windows.
    """

    # this is purely nominal and must be overloaded in subclasses
    group = Enum(_all_groups[0], _all_groups)
    session_name = Str

    session_txt = Code
    show_session_txt = Bool

    proc_runs = List
    units = Str('uV')
    auto_prune = Bool(False)
    manu_prune = Bool(False)
    normalize = Bool(False)
    car = Bool(False)
    integrate_amps = Bool(False)
    lowpass_env = Bool(False)
    env_bandwidth = Float

    proc_bandpass = Tuple(2.0, 150.0)
    proc_notches = Str
    proc = Button('Analyze')

    # scroll_len = Float(2.0)
    scroll = Button('Scroller')
    _can_use_scroller = Bool(False)

    ana_class = Analysis
    ana_modules = [ChannelAnalysis,
                   AverageAnalysis,
                   FrequencyAnalysis,
                   ArrayMaps,
                   Spectrogram,
                   StimDecoding,
                   Filters]


    def __init__(self, **traits):
        # a length-1 working set of data
        self._dset_cache = (None, None)
        super(AnalysisSetup, self).__init__(**traits)
        self._can_use_scroller = _SCROLLER

    @property
    def session(self):
        return '/'.join([self.group, self.session_name])

    @on_trait_change('show_session_txt, session')
    def _load_session_text(self):
        if self.show_session_txt:
            try:
                p = find_conf(self.session)
                self.session_txt = open(p).read()
            except BaseException:
                self.session_txt = ''
                self.show_session_txt = False
        else:
            self.session_txt = ''

    def _get_dataset_params(self):
        """
        Returns "common" dataset loading parameters:

        * session (non-optional)
        * list-of-runs (non-optional)
        * keyword dictionary:
          + bandpass settings
          + notch filter freqs
          + snip_transient (t/f)
          + units (string code)

        Downstream objects can add additional keywords to the dictionary
        """

        if not len(self.proc_runs):
            raise EmptyProcSet
        runs = [r.split(',')[0] for r in self.proc_runs]
        if self.proc_notches:
            notches = list(map(float, self.proc_notches.split(',')))
        else:
            notches = ()
        # string together the args and kwargs used to load the data,
        # and compare it against the working memory set

        args = (self.session, runs)
        kwargs = dict(
            bandpass=self.proc_bandpass, notches=notches,
            snip_transient=True, units=self.units
        )
        post_proc = dict(
            auto_prune=self.auto_prune, manu_prune=self.manu_prune,
            car=self.car, normalize=self.normalize,
            integrate_amps=self.integrate_amps
        )
        key = args + (kwargs, post_proc)
        return key

    def ana_opts(self):
        return dict()

    def _get_dataset(self):
        params = self._get_dataset_params()

        if params == self._dset_cache[0]:
            return self._dset_cache[1]

        session, runs, kwargs, post_proc = params
        dsets = [load_experiment_auto(session, r, **kwargs) for r in runs]
        for d in dsets:
            if self.auto_prune:
                rms = safe_avg_power(d.data.iter_blocks(), int(d.Fs), iqr_thresh=8)
                # slightly conservative 4x IQR
                mask = bad_channel_mask(np.log(rms), iqr=4)
                d.data.set_channel_mask(mask)
                d.chan_map = d.chan_map.subset(mask.nonzero()[0])
            elif self.manu_prune:
                from ecoglib.signal_testing.channel_picker import interactive_mask
                mask = interactive_mask(d)
                d.data.set_channel_mask(mask)
                d.chan_map = d.chan_map.subset(mask)
        if self.integrate_amps:
            for d in dsets:
                session = d.name.split('.')[0]
                d.data = convert_amps_to_volts(d, to_unit='uV').data
                # highpass filter again
                ## bp = kwargs['bandpass']
                # d.data = filter_array(
                ##     d.data, inplace=False,
                ##     design_kwargs=dict(lo=bp[0], hi=bp[1], Fs=d.Fs)
                # )
                d.units = 'uV'
        if self.normalize:
            for d in dsets:
                rms = safe_avg_power(d.data.iter_blocks(), int(d.Fs))
                d.data /= rms[:, None]
                d.units = 'sigma'
        dataset = join_datasets(dsets)
        if self.car:
            dataset.data.regress_common_average()
        if self.lowpass_env:
            # hilbert transform the data, and then lowpass at the
            # width of the set bandpass
            print('computing lowpass envelope')
            if self.env_bandwidth <= 0:
                # default value of 1.25X width of bandpass
                lp = 1.25 * (self.proc_bandpass[1] - self.proc_bandpass[0])
            else:
                lp = self.env_bandwidth
            Fs = dataset.Fs
            # 2-second blocks for lowpass envelope
            dataset.data = lowpass_envelope(dataset.data, int(2 * Fs), min(1, 2 * lp / Fs))
        self._dset_cache = (params, dataset)
        gc.collect()
        return dataset

    def _scroll_fired(self):
        scr = create_scroll_launcher(self)
        scr.configure_traits()
        return scr

    def _proc_fired(self):
        try:
            dataset = self._get_dataset()
        except EmptyProcSet:
            e = Error(error_msg='There are no recordings in "to process"')
            e.edit_traits()
            return
        ana = self.ana_class(dataset, modules=self.ana_modules, **self.ana_opts())
        ana.configure_traits()
        return ana

    view = View(
        HGroup(
            VGroup(Item('group'),
                   Item('session_name', label='session', editor=EnumEditor(name='handler.available_sessions'))),
            Item('show_session_txt', label='Show session config')
        ),
        Item('_'),
        Item(name='proc_runs', show_label=False,
             editor=SetEditor(name='handler.available_runs',
                              left_column_title='session runs',
                              right_column_title='to process')
             ),
        Item('_'),
        HGroup(
            VGroup(
                Item('proc_bandpass', style='simple', label='Bandpass Edges'),
                Item(
                    'proc_notches', style='simple', label='Notch freqs',
                    help='comma-separated list of notches'
                )
            ),
            spring,
            VGroup(
                Item('units', width=3),
                HGroup(
                    VGroup(
                        Item('integrate_amps', label='Integrate'),
                        Item('lowpass_env', label='LP Envelope'),
                        Item('env_bandwidth', label='Envelope BW (Hz)', visible_when='lowpass_env')
                    ),
                    VGroup(
                        Item('auto_prune', label='Auto Reject Chans'),
                        Item('manu_prune', label='Manually Prune Chans'),
                        Item('car', label='Com Avg Reref'),
                        Item('normalize', label='Normalize')
                    )
                )
            ),
            spring,
            VGroup(
                Item(name='proc', label='Tonotopy'),
                Item('_'),
                HGroup(
                    Item('scroll', enabled_when='_can_use_scroller')
                )
            )
        ),
        Group(
            Item(
                'session_txt', style='readonly', label='Config',
                enabled_when='show_session_txt'
            ),
            visible_when='show_session_txt'
        ),
        title='Analysis Helper',
        resizable=True,
        handler=SessionHandler
    )


