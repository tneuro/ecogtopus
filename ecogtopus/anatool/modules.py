import os
import os.path as osp
from collections import OrderedDict
from itertools import product
from traits.api import HasTraits, Enum, List, Instance, Button, Int, \
    Float, Bool, on_trait_change, Range, Tuple, Property, \
    property_depends_on
from traitsui.api import View, Item, UItem, EnumEditor, \
    VGroup, HGroup, CheckListEditor, Label
from matplotlib import rc_context, rcParams
from matplotlib.gridspec import GridSpec
from matplotlib import cm
from matplotlib import ticker
import numpy as np

import ecoglib.vis.colormaps as colormaps
from ecoglib.vis import plotters
from ecoglib.vis.tile_images import tile_traces_1ax
from ecoglib.estimation.multitaper import mtm_spectrogram_basic, mtm_spectrogram
from ecoglib.vis.gui_tools import SavesFigure, current_screen
from ecoglib.vis import plot_util as pu
from ecogdata.parallel.split_methods import multi_taper_psd
from ecogdata.expconfig.exp_descr import StimulatedExperiment, ordered_epochs
from ecogdata.util import get_default_args, equalize_groups, nextpow2, fenced_out, nanpercentile
from ecogdata.channel_map import CoordinateChannelMap

from .. import GUI_MODE
if GUI_MODE:
    from ecoglib.vis.plot_util import subplots
else:
    from matplotlib.pyplot import subplots
from ..ep_scoring import peak_lags, roc_test, evoked_snr
from ..decoding import prepare_samples, confusion_matrix_plot, auc_matrix_plot, plot_roc_bunch, plot_roc, Decoder


__all__ = ['WrapsExperiment', 'AnalysisModule', 'ChannelAnalysis',
           'AverageAnalysis', 'FrequencyAnalysis', 'ArrayMaps',
           'Spectrogram', 'StimDecoding']


class WrapsExperiment(HasTraits):
    """
    An object to expose and select stimulation parameters.

    This class takes a StimulatedExperiment object and automatically
    creates Trait-ed attributes of the stim parameters used to enumerate
    unique condition labels.

    """

    _n_conditions = Int
    _all_conditions = List
    exp = Instance(StimulatedExperiment)

    def __init__(self, experiment, condition_labels=dict(), **traits):
        """
        experiment is a StimulatedExperiment
        """

        super(WrapsExperiment, self).__init__(**traits)
        # add traits based on experiment tabs
        for tab in experiment.enum_tables:
            # make a trait for the values
            values = np.unique(getattr(experiment, tab))
            self.add_trait(tab, List(value=values))

            # make a simple attribute for the labels
            # someone downstream knows how to do this
            labels = condition_labels.get(tab, ())
            if not len(labels):
                labels = [str(v) for v in values]
            setattr(self, tab + '_labels', labels)

            # make a trait for the index mapping
            map_list = List(value=list(enumerate(labels)))
            self.add_trait('_' + tab + '_map', map_list)

            # finally add a trait for the index itself
            idx_list = List(editor=CheckListEditor(name='_' + tab + '_map'))
            self.add_trait('_' + tab + '_idx', idx_list)

            self.on_trait_change(self._update_index, '_' + tab + '_idx')

        self.exp = experiment
        self._n_conditions = len(experiment.enum_tables)
        self._all_conditions = list(experiment.enum_tables)
        for tab in experiment.enum_tables:
            setattr(self, '_' + tab + '_idx', [0])

        self.add_trait(
            '_condition_groups',
            List(editor=CheckListEditor(
                values=experiment.enum_tables, cols=len(experiment.enum_tables)
            ))
        )
        self.add_trait(
            'conditions',
            List(editor=CheckListEditor(values=experiment.enum_tables))
        )
        self.conditions = list(experiment.enum_tables[:1])

    def index(self, tab):
        idx = getattr(self, '_' + tab + '_idx')[0]
        return idx

    def values(self, tab):
        return getattr(self, tab)

    def value(self, tab):
        return self.values(tab)[self.index(tab)]

    def set_value(self, tab, value):
        index = self.values(tab).index(value)
        setattr(self, '_' + tab + '_idx', [index])

    def labels(self, tab):
        return getattr(self, tab + '_labels')

    def label(self, tab):
        return self.labels(tab)[self.index(tab)]

    def _update_index(self):
        exp = self.exp
        for tab in exp.enum_tables:
            try:
                setattr(self, tab + '_label', self.label(tab))
                setattr(self, tab + '_value', self.value(tab))
            except BaseException:
                pass

    def current_conditions(self, floating=(), as_path=False):
        """Build label (string) for the present condition set"""
        if isinstance(floating, str):
            floating = (floating,)
        labels = [(p.replace('_', ' '), self.label(p))
                  for p in self.exp.enum_tables
                  if p not in floating]

        labels = ' '.join(['{0}: {1}'.format(*p) for p in labels])
        if as_path:
            labels = labels.replace(':', '').replace(' ', '_').replace('.', '_')
        return labels

    def _condition_list_factory(self):
        tables = self.exp.enum_tables
        lists = OrderedDict()
        for tab in tables:
            lists[tab] = Item(
                '_{0}_idx'.format(tab), style='simple',
                label=tab.replace('_', ' ')
            )
        return lists

    def default_traits_view(self):
        param_handles = self._condition_list_factory()
        v_args = param_handles.values()
        return View(*v_args, resizable=True, title='experiment conditions')


class AnalysisModule(WrapsExperiment):
    """
    A generic type with parameters common to any analysis module.

    Instantiating the object requires a general Analysis object,
    which reveals the session, runs, and loaded dataset+experiment.
    The Analysis object also can provide friendly labels for the
    condition parameters.
    """

    name = '__no-name__'

    ana = Instance('ecogtopus.anatool.base.Analysis')

    # these are specified in seconds, relative to the tone onset
    # (these will be sync'd with master object?)
    epoch_start = Float
    epoch_end = Float

    peak_min = Float
    peak_max = Float

    # --- For interal use ---
    selected_site = Int(-1)

    synced_traits = ('epoch_start', 'epoch_end',
                     'peak_min', 'peak_max',
                     'selected_site', 'exp')

    def __init__(self, session_analysis, condition_labels=dict(), **traits):
        """
        session_analysis is an Analysis object

        All AnalysisModules get instantiated by the session_analysis
        "parent" object, which should know the labels of the conditions.
        If not, then just naively convert condition values to strings.
        """
        exp = session_analysis.exp
        super(AnalysisModule, self).__init__(
            exp, condition_labels=condition_labels, **traits
        )
        self.ana = session_analysis
        self.dataset = self.ana.dataset

        # sync with parent (one way?)
        for trait_name in self.synced_traits:
            d = dict([(trait_name, getattr(session_analysis, trait_name))])
            self.trait_setq(**d)
            self.sync_trait(trait_name, session_analysis, mutual=True)

    @property
    def spath(self):
        session = self.ana.dataset.name.split('.')[0]
        cwd = osp.join(os.getcwd(), session)
        cwd = osp.join(cwd, self.ana.dataset.name)
        return osp.join(cwd, '_'.join(self.name.split()))

    def condition_colors(self, condition, level=None):
        values = self.values(condition)
        n_colors = len(values)
        colors = self.ana._colors[condition](np.linspace(0, 1, n_colors))
        if level is not None:
            return colors[values.index(level)]
        return colors

    def slice_for_rolled_average(self, resp_avg=None, tabs=None):
        """
        Returns a permutation order and a slicing object based on
        the currently chosen variable conditions. This indexing
        pair can be used to slice into an array with the rolled
        condition shape of (n_chan,) + rolled_shape + [(n_pts,)]

        resp_avg.transpose(ax_order)[avg_slicing]

        If tables are provided (in tabs) then all values along those
        axes will be returned in the slice, in the order specified.
        If tabs is None, the slicing returns a single value at each axis.
        The _condition_groups list will be tried if tabs is empty.

        If a response average array is provided, then the sliced array
        will be returned.

        Returns
        -------

        r_sl, ax_order : the response slicer and the axes permutation order
        """

        # first get axes permutation
        if tabs is None:
            tabs = ()
        elif not len(tabs):
            tabs = self._condition_groups
        exp = self.exp
        fixed_tabs = [t for t in exp.enum_tables if t not in tabs]
        ax_order = [exp.enum_tables.index(t) + 1 for t in tabs]
        ax_order.extend(
            [exp.enum_tables.index(t) + 1 for t in fixed_tabs]
        )
        ax_order.insert(0, 0)

        # keep timeseries dim last
        ax_order.append(len(ax_order))

        fixed_slices = [self.values(t).index(self.value(t))
                        for t in fixed_tabs]
        r_sl = [slice(None)] * len(tabs) + fixed_slices

        # append channel slicing
        if self.ana.selected_site >= 0:
            r_sl.insert(0, self.ana.selected_site)
        else:
            r_sl.insert(0, slice(None))
        r_sl = tuple(r_sl)
        if resp_avg is not None:
            resp_avg = resp_avg.transpose(ax_order[:resp_avg.ndim])
            return resp_avg[r_sl]

        return r_sl, ax_order


class ChannelAnalysis(AnalysisModule):
    """
    This type exposes plotting functionality for exploring responses on
    a site-by-site basis.
    """

    name = 'Channel Analysis'

    trials_as_heatmaps = Bool
    _repeat_conditions = Property(depends_on='conditions')
    repeat_tuning = Enum(values='_repeat_conditions')
    plot_tuning = Button('Plot Tuning')

    def __init__(self, *args, **traits):
        super(ChannelAnalysis, self).__init__(*args, **traits)
        # generate the buttons from the experiment's enum ranking
        exp = self.ana.exp
        # self.repeat_tuning = exp.enum_tables
        self.buttons = dict()
        for t in exp.enum_tables:
            # make a button to plot @ the floating condition
            bname = 'plot_{0}'.format(t)
            self.add_trait(
                bname,
                Button('Plot all {0}'.format(t.replace('_', ' ')))
            )
            self.on_trait_event(self._pre_make_plot, bname)
            self.buttons[t] = bname

    def _get__repeat_conditions(self):
        return [c for c in self._all_conditions if c not in self.conditions]

    def _pre_make_plot(self, name, val, **kwargs):
        if self.selected_site < 0:
            print('Select a site first')
            return
        name = name.split('plot_')[1]
        fig = self.channel_plot(name, **kwargs)
        labels = self.current_conditions(floating=name, as_path=True)
        save_str = 'chan_{:02d}_'.format(self.selected_site) + labels
        if self.trials_as_heatmaps:
            save_str += '_heatmap'
        # fig.canvas = None#
        return SavesFigure.live_fig(
            fig, sfile=save_str, spath=self.spath
        )

    def channel_plot(self, floating_condition, clim=(), cmap=None):
        i, j = self.dataset.chan_map.subset([self.selected_site]).to_mat()

        exp = self.exp
        fixed_values = [(param, self.value(param))
                        for param in exp.enum_tables]
        floats = exp.enum_tables.index(floating_condition)
        fixed_values.pop(floats)

        labels = self.current_conditions(floating=floating_condition)

        plt_str = 'Chan %02d, Site (%d, %d) ' % (self.selected_site, i[0], j[0])
        plt_str = plt_str + labels

        # it's possible there's one condition @ one value, in which
        # case fixed_values is empty and would evaluate as false
        if not len(fixed_values):
            fixed_values = True
        epochs, groups = self.ana.epochs(
            chan=self.selected_site, arranged=fixed_values, mask_outliers=True
        )

        varied = getattr(self, floating_condition)
        a_labels = getattr(self, floating_condition + '_labels')
        b_label = labels

        epochs = epochs.squeeze()
        tx = self.ana.timebase()
        cgroups = np.r_[0, np.cumsum(groups)]

        if self.trials_as_heatmaps:
            # mn, mx = np.percentile(epochs.ravel(), (1e-2, 100-1e-2))
            if not len(clim):
                mn, mx = np.percentile(epochs.ravel(), (0.5, 99.5))
            else:
                mn, mx = clim
            if not cmap:
                if mn < 0 and mx > 0:
                    cmap = colormaps.diverging_cm(mn, mx)
                elif mn < 0 and mx < 0:
                    cmap = plotters.sns.blend_palette(
                        ['blue', 'white'], n_colors=cm.jet.N, as_cmap=True
                    )
                else:
                    cmap = plotters.sns.blend_palette(
                        ['white', 'red'], n_colors=cm.jet.N, as_cmap=True
                    )
            # this only currently works with equal group sizes
            mx_rep = max(groups)
            try:
                epochs.shape = (len(varied), mx_rep, -1)
            except BaseException:
                epochs_fill = np.empty(
                    (len(varied), mx_rep, epochs.shape[-1]),
                    dtype=epochs.dtype
                )
                epochs_fill.fill(np.nan)
                for n in range(len(groups)):
                    g = epochs[cgroups[n]:cgroups[n + 1]]
                    epochs_fill[n, :groups[n], :] = g
                epochs = epochs_fill

            fig = pu.stacked_epochs_image(
                tx, epochs, a_labels, b_label,
                np.arange(mx_rep, len(groups) * mx_rep, mx_rep),
                tm=0, stacked='cond_a', bcolor=(.4, .4, .4),
                cmap=cmap, clim=(mn, mx),
                title=plt_str, clabel=self.ana._nice_unit
            )
        else:
            group_samps = list()
            for n in range(len(cgroups) - 1):
                group_samps.append(epochs[cgroups[n]:cgroups[n + 1]])
            fig = pu.stacked_epochs_traces(
                tx, group_samps, a_labels, title=plt_str, tm=0,
                calib_unit=self.dataset.units
            )
        return fig

    def _plot_tuning_fired(self):
        floating = self.conditions[0]
        # In the 2-label case, the traits property forces the binary choice
        if self._n_conditions > 1:
            repeating = self.repeat_tuning
        else:
            repeating = None
        f = self.plot_tuning_curve(floating, repeat_for=repeating)

        save_str = 'chan_{0:02d}_{1}_tuning_curves'.format(self.selected_site,
                                                           self.ana.scoring)
        # f.canvas = None
        return SavesFigure.live_fig(
            f, sfile=save_str, spath=self.spath
        )

    def plot_tuning_curve(self, floating_condition=None, repeat_for=None):
        ana = self.ana
        if ana.selected_site < 0:
            print('Select a site and plot again!')
            return
        # gets "epochs" (time windows) in order of condition,
        # and the size of each condition group
        epochs, gs = ana.epochs(arranged=True)

        # just score every timeseries
        score = ana.score_fn(r=epochs)  # [ana.selected_site]

        if ana.epoch_outlier_threshold > 0:
            rms = epochs[ana.selected_site].std(axis=-1)
            t = ana.epoch_outlier_threshold
            mask = fenced_out(np.log(rms), thresh=t, axis=-1)
            # score[~mask] = np.nan
            np.putmask(score[ana.selected_site], ~mask, np.nan)

        # breaks up the trial dimension into # conditions by # trials
        # (and if necessarry fills with nan)
        score = equalize_groups(score, gs, axis=1, reshape=True)
        score = np.ma.masked_invalid(score)
        # breaks up # conditions dimension into (..., #condA, #condB, #condC, ...)
        score.shape = (len(score),) + self.exp.rolled_conditions_shape() + (-1,)

        # Tuning is plotted over this condition (roll it to the front)
        if floating_condition is None:
            t_cond = self.conditions[0]
        else:
            t_cond = floating_condition

        if self._n_conditions > 1 and repeat_for is not None:
            # choose which condition to "repeat over" -- any other conditions will be fixed at their current values
            # determined by the selection boxes
            repeating = True
            r_cond = repeat_for
            # This returns slice and axis permutation that includes channels and post-condition dims (repeats)..
            # slicing out the current channel here
            score = self.slice_for_rolled_average(tabs=(r_cond, t_cond), resp_avg=score)
            assert score.ndim == 3, 'wtf'
            curve_colors = self.condition_colors(r_cond)
            r_value_restore = self.value(r_cond)

        else:
            repeating = False
            score = self.slice_for_rolled_average(tabs=(t_cond,), resp_avg=score)
            score = score[np.newaxis, ...]
            curve_colors = (None,)


        # mean and sem
        mn = score.mean(-1)
        se = score.std(-1)
        # normalize by the sqrt of un-masked trials
        se /= np.sqrt((~score.mask).sum(axis=-1))

        # make the plot
        site = ana.selected_site
        i, j = ana.dataset.chan_map.rlookup(site)
        with plotters.sns.plotting_context('notebook', font_scale=0.8):
            x_labels = self.labels(t_cond)
            x_axis = np.arange(len(x_labels))
            f, ax = subplots(figsize=(3, 3))
            num_curves = len(mn)
            for i in range(num_curves):
                if repeating:
                    self.set_value(r_cond, self.values(r_cond)[i])
                    label = self.current_conditions(floating=t_cond)
                else:
                    label = ''
                color = curve_colors[i]
                ax.plot(x_axis, mn[i], color=color, label=label)
                ax.errorbar(x_axis, mn[i], yerr=se[i], fmt='none', ecolor='k')
            if repeating:
                ax.legend()
                self.set_value(r_cond, r_value_restore)
            ax.set_xticks(np.arange(0, len(x_labels), 3))
            ax.set_xticklabels([x_labels[int(t)] for t in ax.get_xticks()])
            ax.set_xlabel(t_cond)
            ax.set_ylabel('EP score ({0})'.format(ana.scoring))
            ax.set_xlim(-0.5, len(x_axis) - 0.5)
            plotters.sns.despine(fig=f)
            ax.set_title('Site {2} ({0}, {1})'.format(i, j, site))
            # f.tight_layout()
        return f

    def default_traits_view(self):
        param_handles = self._condition_list_factory()
        button_groups = list()
        for tab in self.exp.enum_tables:
            button_groups.append(
                VGroup(param_handles[tab], UItem(self.buttons[tab]))
            )

        tune_curve = VGroup(Label('Tune Condition'),
                            UItem('conditions', style='simple'),
                            UItem('plot_tuning'))
        button_groups.append(tune_curve)
        # If there are more than 2 condition labels, need to choose which one to repeat the curve for
        if len(self.exp.enum_tables) > 2:
            tune_repeat = VGroup(Label('Repeat Condition'),
                                 UItem('repeat_tuning', style='simple'))
            button_groups.append(tune_repeat)

        v = View(
            VGroup(
                HGroup(*button_groups),
                HGroup(
                    Item('epoch_start', label='Epoch Start'),
                    Item('epoch_end', label='Epoch End'),
                    Item('trials_as_heatmaps', label='Heatmap')
                )
            ),
            resizable=True, title='Trial EPs'
        )
        return v


class AverageAnalysis(AnalysisModule):
    """
    This object provides the following plots:

    * The average responses of each channel to a particular condition value,
      plotted at the correct array sites
    * The average responses of one channel for a 2D table of condition
      values

    """

    name = 'Average EPs'

    plot_array_avg_b = Button('Array EPs')
    plot_site_table_b = Button('EP Table')
    fix_scale = Bool(True)
    y_err = Enum('sem', ('sem', 'stdev', 'pctile', 'plot all'))

    def __init__(self, *args, **kwargs):
        super(AverageAnalysis, self).__init__(*args, **kwargs)
        tables = self.exp.enum_tables
        if len(tables) < 3:
            self._condition_groups = list(tables)

    def _correct_default_tilesize(self, geometry):
        # if default tile size is too big, let maximum figure
        # height be 2/3 of the height of the screen
        def_tile = get_default_args(tile_traces_1ax)['tilesize']
        fig_dpi = rcParams['figure.dpi']
        mx_tile_ht = current_screen.y * 2 / 3. / geometry[0] / fig_dpi
        mx_tile_wd = current_screen.x / geometry[1] / fig_dpi
        tile = min(mx_tile_ht, mx_tile_wd, def_tile[0])
        return (tile, tile)

    def _plot_site_table_b_fired(self, **kwargs):
        save_str = 'chan_%02d_ep_table' % self.selected_site
        fig = self.plot_site_table(**kwargs)
        # fig.canvas = None
        return SavesFigure.live_fig(
            fig, sfile=save_str, spath=self.spath
        )

    def plot_site_table(self, title='', tabs=(), tilesize=None):
        """
        If tabs is specified, then it must be a sequence of length-2.
        These condition parameters vary in the vertical and horizontal
        table direction, respectively.

        """
        site = self.selected_site
        i, j = self.dataset.chan_map.subset([site]).to_mat()

        if not title:
            title = 'EPs Chan %02d, Site (%d, %d)' % (site, i[0], j[0])

        # get all eps, and find out how to slice
        ep = self.ana.ep_average(reshape=True)
        # first get axes permutation
        if not tabs:
            tabs = self._condition_groups
        if len(tabs) != 2:
            raise ValueError('two condition groups must be chosen')

        table_eps = self.slice_for_rolled_average(resp_avg=ep, tabs=tabs)
        geo = table_eps.shape[:2]
        table_eps = table_eps.reshape(geo[0] * geo[-1], -1)

        if self.fix_scale:
            mn = np.nanmin(self.ana.ep_average())
            mx = np.nanmax(self.ana.ep_average())
        else:
            mn = np.nanmin(table_eps)
            mx = np.nanmax(table_eps)

        if tilesize is None:
            tilesize = self._correct_default_tilesize(geo)

        fig = tile_traces_1ax(
            table_eps, geo=geo, twin=(self.epoch_start, self.epoch_end),
            col_major=False, yl=(mn, mx),
            title=title, calib_unit=self.dataset.units,
            plot_style='single', table_style='cartesian',
            x_labels=self.labels(tabs[1]),
            y_labels=self.labels(tabs[0]),
            tilesize=tilesize
        )
        return fig

    def _plot_array_avg_b_fired(self, **kwargs):
        save_str = self.current_conditions(as_path=True) + '_array_ep'
        fig = self.plot_array_avg(**kwargs)
        # fig.canvas = None
        return SavesFigure.live_fig(
            fig, sfile=save_str, spath=self.spath
        )

    def plot_array_avg(self, title='', tilesize=None):

        exp = self.exp
        fixed_values = [(param, self.value(param))
                        for param in exp.enum_tables]
        ep_mask = ordered_epochs(exp, fixed_values)
        epochs = self.ana.epochs(epochs_mask=ep_mask, mask_outliers=True)
        try:
            # Try nan-filling masking trials:
            # this is compatible with tile_traces function
            epochs = epochs.filled(np.nan)
        except AttributeError:
            # if no masking, then proceed
            pass
        ep = self.ana.ep_average(reshape=True)
        if not self.fix_scale:
            yl = None
        else:
            sig = np.nanstd(ep)
            mn = np.nanmin(ep) - sig
            mx = np.nanmax(ep) + sig
            yl = (mn, mx)

        style_map = {'pctile': 'sample', 'plot all': 'all'}
        style = style_map.get(self.y_err, self.y_err)

        if not title:
            # this is a hook to allow a subclass to specify a
            # better title
            title = 'Mean EP +/- %s : %s' % (
                self.y_err, self.current_conditions()
            )

        # TODO: fix for coord maps
        if tilesize is None:
            geo = self.dataset.chan_map.geometry
            tilesize = self._correct_default_tilesize(geo)

        fig = tile_traces_1ax(
            epochs, p=self.dataset.chan_map, plot_style=style,
            twin=(self.epoch_start, self.epoch_end), title=title, yl=yl,
            calib_unit=self.dataset.units, tilesize=tilesize
        )
        return fig

    def default_traits_view(self):
        param_handles = self._condition_list_factory()
        # condition value toggles group
        p_group = VGroup(*param_handles.values())
        # control panel group
        c_group = VGroup(
            Item('fix_scale', label='Fix Y-scale'),
            Item('y_err', label='Error')
        )
        # button group
        b_group = VGroup(
            UItem('plot_array_avg_b'),
            UItem('plot_site_table_b')
        )

        v = View(
            VGroup(
                HGroup(p_group, Item('_'), c_group, Item('_'), b_group),
                HGroup(
                    Item('epoch_start', style='simple', label='Epoch start'),
                    Item('epoch_end', style='simple', label='Epoch end'),
                    VGroup(
                        Item('_condition_groups', style='custom',
                             label='Conditions for table',
                             enabled_when='_n_conditions>2'),
                        Item('_condition_groups', style='readonly',
                             label='Table (row, col)')
                    )
                )
            ),
            resizable=True, title='Average EPs'
        )
        return v


class ArrayMaps(AnalysisModule):
    """
    Makes array maps of response magnitude.
    """

    name = 'Array Maps'
    mag_maps = Button('Plot Mag')
    energy_map = Button('Energy Projection')
    blend_map = Button('Plot Blended')
    esnr_map = Button('Plot ESNR')
    save_esnr = Button('Save ESNR')
    dprime_map = Button('Plot d-prime')
    array_roc_button = Button('Plot ROC')
    single_trial = Bool(True)
    correct_esnr_bias = Bool(True)
    lags = Enum(1, range(1, 10))

    def __init__(self, *args, **kwargs):
        super(ArrayMaps, self).__init__(*args, **kwargs)
        self._coord_map = isinstance(self.ana.dataset.chan_map, CoordinateChannelMap)

    def _mag_maps_fired(self):
        map_condition = self.conditions[0]
        fig = self.plot_maps(map_condition)
        # fig.canvas = None
        labels = self.current_conditions(floating=self.conditions[0])
        save_str = '{0}_maps_{1}'.format(self.ana.scoring, labels)
        return SavesFigure.live_fig(
            fig, sfile=save_str, spath=self.spath
        )

    def condition_maps(self, condition):
        """
        Make array maps for each level of the given condition
        based on the current response score of the parent object.
        """
        # Make maps
        scores = self.ana.ep_score(single_trials=self.single_trial, avg_single_trials=True)
        fixed_indices = [self.index(cond) for cond in self.exp.enum_tables]
        score_slice = [slice(None)] + fixed_indices
        s_idx = self.exp.enum_tables.index(condition) + 1
        n_values = len(self.values(condition))
        fields = np.empty((n_values, scores.shape[0]), scores.dtype)
        for v in range(n_values):
            score_slice[s_idx] = v
            fields[v] = scores[tuple(score_slice)]

        # Convert to RGBA
        if isinstance(self.ana._colors[condition], type(cm.jet)):
            colors = self.ana._colors[condition](np.linspace(0, 1, n_values))
        else:
            colors = self.ana._colors[condition]
        n = cm.jet.N
        cmaps = [
            plotters.sns.blend_palette(['white', c], n_colors=n, as_cmap=True)
            for c in colors]
        # contrast enhancement
        clim = np.percentile(fields, (2, 98))

        rgba_fields = [colormaps.rgba_field(cmap, f, clim=clim)[0]
                       for cmap, f in zip(cmaps, fields)]
        # make a dummy field with a white-to-black map for colorbar
        bw_map = plotters.sns.blend_palette(
            ['white', 'black'], n_colors=n, as_cmap=True
        )
        cb_fun = colormaps.rgba_field(bw_map, fields[-1], clim=clim)[1]
        return rgba_fields, cb_fun

    def plot_maps(self, condition):
        """
        Plot the array maps made in condition_maps()
        """

        n_values = len(self.values(condition))
        rgba_fields, cb_fun = self.condition_maps(condition)

        # Labeling
        scoring = self.ana.scoring
        score_label = dict([
            ('ptp', 'Peak-to-Peak'), ('pk-bl', 'Peak-to-baseline'),
            ('rms', 'RMS'), ('mahal', 'Mahalanobis distance')
        ]).get(scoring)

        labels = self.current_conditions(floating=condition)
        plt_str = '{0} EP @ {1}'.format(score_label, labels)

        # Plotting
        fig, axs = subplots(1, n_values, figsize=(n_values * 1, 3.25), sharex=True, sharey=True)

        chan_map = self.dataset.chan_map
        labels = self.labels(condition)
        if n_values == 1:
            axs = [axs]
        for n in range(n_values):
            ax = axs[n]
            if self._coord_map:
                colors = rgba_fields[n].astype('d') / 255.0
                # TODO: size of points is just hard coded -- need a better way to dynamically scale
                chan_map.image(cbar=False, ax=ax, scatter_kw=dict(c=colors, s=25))
                ax.axis('image')
            else:
                ax.imshow(chan_map.embed(rgba_fields[n], axis=0).astype('B'), origin='upper')
            ax.axis('off')
            ax.set_title(labels[n], fontsize=12)
        # fig.tight_layout()
        # fig.canvas.draw()
        y0 = max(0.2, ax.get_position().y0)
        # make room for titles at the top
        y1 = 1 - fig.transFigure.inverted().get_matrix()[1, 1] * (18 + 12)
        fig.subplots_adjust(bottom=y0, top=y1, wspace=0.1)
        cax = fig.add_axes([0.25, y0 - 0.075, 0.5, 0.05])
        cb_fun(cax, orientation='horizontal')
        # fig.canvas.draw()
        xtl = cax.get_xticklabels()
        xtl[0].set_text(r'$\le $' + xtl[0].get_text())
        xtl[-1].set_text(r'$\ge $' + xtl[-1].get_text())
        cax.set_xlabel(
            r'{0} ({1})'.format(scoring, self.ana._nice_unit),
            labelpad=2, fontsize=12
        )
        [sp.set_visible(False) for sp in cax.spines.values()]
        fig.suptitle(plt_str, fontsize=18)
        return fig

    def _blend_map_fired(self):
        fig = self.plot_blended(self.conditions[0])
        # fig.canvas = None
        labels = self.current_conditions(floating=self.conditions[0])
        save_str = '{0}_blended_{1}'.format(self.ana.scoring, labels)
        return SavesFigure.live_fig(
            fig, sfile=save_str, spath=self.spath
        )

    def plot_blended(self, condition):
        """
        Plot an image that blends all the condition maps through the
        "darkening" operation.
        """

        rgba_fields, cb_fun = self.condition_maps(condition)
        fig = plotters.plt.figure(figsize=(6, 6))
        ax = fig.add_subplot(111)
        blended = np.array(rgba_fields).min(axis=0)
        if self._coord_map:
            blended = blended.astype('d') / 255.0
            self.ana.dataset.chan_map.image(cbar=False, ax=ax, scatter_kw=dict(c=blended))
        else:
            blended = self.ana.dataset.chan_map.embed(blended, axis=0).astype('B')
            ax.imshow(blended, origin='upper')
        labels = self.current_conditions(floating=self.conditions[0])
        ax.set_title('Blended {0} scores: {1}'.format(self.ana.scoring, labels))
        # fig.tight_layout()
        return fig

    def calc_array_roc(self, lags=1, **kwargs):
        from numpy.lib.index_tricks import as_strided
        r0, r1 = self.ana._epoch_window(samps=True, subresponse=True)

        pos_edge = (self.exp.time_stamps - r0).astype('i')
        neg_edge = (self.exp.time_stamps + r1).astype('i')

        signal = self.ana.dataset.data
        # want a total of ~ 5x nchan * lags samples, and each point in
        # the epoch window counts as a sample

        num_samps = float(5 * len(signal) * lags) / int(r1 + r0)
        baseline = self.ana.baseline_samples(n=int(num_samps))
        if lags > 1:
            # the new "channel" dimension will be
            # channel x lags ( in that order )
            if not signal.flags.c_contiguous:
                signal = signal.copy()
            shp = [signal.shape[0], signal.shape[1] - lags + 1, lags]
            strides = [signal.strides[0], signal.strides[1], signal.strides[1]]
            signal = as_strided(signal, shape=shp, strides=strides).copy()
            signal = signal.transpose(1, 0, 2).copy().reshape(shp[1], -1)

            shp = [baseline.shape[0], baseline.shape[1],
                   baseline.shape[2] - lags + 1, lags]
            strides = [baseline.strides[0], baseline.strides[1],
                       baseline.strides[2], baseline.strides[2]]
            baseline = as_strided(baseline, shape=shp, strides=strides).copy()
            baseline = baseline.transpose(1, 2, 0, 3).copy()
            baseline.shape = (-1, signal.shape[-1])
        else:
            baseline = baseline.reshape(len(baseline), -1).transpose()
            signal = signal.transpose()

        return roc_test(
            signal, baseline, (pos_edge, neg_edge), **kwargs
        )

    def calc_esnr(self, single_trial=True, squared=True, db=True, avg_samps=True,
                  use_baseline=False, correct_bias=True, iqr_thresh=-1):
        """
        Calculate the ratio of the highest evoked-response score
        relative to the background level. This is termed
        evoked-signal-to-noise (ESNR). This object's current evoked
        response score is used.

        Parameters
        ----------
        single_trial: Bool (default True)
            Calculate scores on single trials
            and use largest average score. Otherwise calculate the scores on
            the trial-averaged responses.
            (single_trial == False) implies (prestim_ref == True)
        squared: Bool (default True)
            The scores are squared, which (under
            scores like RMS and Mahalanobis distance) supports the
            interpretation of ESNR as a ratio of variances.
        db: Bool (default True)
            Returns ESNR in decibels (implies squared==True)
        avg_samps: Bool (default True)
            Returns ESNR statistic averaged over
            multiple trials. Alternatively, return the entire sample.
        use_baseline: Bool (default False)
            Do baseline over baseline SNR (for QA or null hypothesis)
        correct_bias: Bool (default True)
            Use the maximum of medians for SNR denominator, matching the numerator selection.
        iqr_thresh: float (default -1)
            Thresholding parameter. Set to -1 to adopt parent object's default.
            Set to None to disable thresholding.

        Returns
        -------
        esnr: np.ndarray
            an array of ESNR calculated at each site

        """
        # if style is single trial, then the baseline value is the median
        # baseline score
        # if style is average, then the baseline value is ... the same?

        p = 2.0 if squared or db else 1.0
        if iqr_thresh is None:
            iqr_thresh = self.ana.epoch_outlier_threshold

        baseline, groups = self.ana.epochs(baseline=True, arranged=True)
        if use_baseline:
            # permute these for good measure
            sx = np.random.permutation(baseline.shape[1])
            response_trials = np.take(baseline, sx, axis=1)
        else:
            response_trials, groups = self.ana.epochs(arranged=True)

        # "normal" mode is single trials response over baseline:
        # prepare samples and call evoked_snr
        if single_trial:
            resp = self.ana.score_fn(r=response_trials)
            if iqr_thresh > 0:
                rms = response_trials.std(-1)
                mask = fenced_out(np.log(rms), thresh=iqr_thresh)
                np.putmask(resp, ~mask, np.nan)
            np.power(resp, p, resp)

            base = self.ana.score_fn(r=baseline)
            if iqr_thresh > 0:
                if use_baseline:
                    # easiest to just calculate the mask again on the permuted trials
                    rms = baseline.std(-1)
                    mask = fenced_out(np.log(rms), thresh=iqr_thresh)
                np.putmask(base, ~mask, np.nan)
            np.power(base, p, base)

            esnr = evoked_snr(base, resp, groups=groups, correct_bias=correct_bias,
                              db=db, return_trials=not avg_samps)
            return esnr

        # If single trial is not True... this forced a prestim reference before.
        # Punt for now
        raise NotImplemented('ESNR for trial-averaged potentials not supported yet')


    def calc_dprime(self, single_trial=True, estimate_trial_iqr=False,
                    squared=True, db=True, logged=True,
                    control=False, iqr_thresh=-1):
        """
        Get a d' score that is the difference of the maximum-median
        response value and the median baseline value all divided by the
        interquartile range of the response values.
        """

        # cannot estimate distribution of best average EP
        estimate_trial_iqr = estimate_trial_iqr and single_trial

        # get evoked response windows
        if control:
            r, g = self.ana.epochs(baseline=True, arranged=True)
            # shuffle the returned order to remove any possible structure
            sx = np.random.permutation(np.arange(r.shape[1]))
            r = np.take(r, sx, axis=1)
        else:
            r, g = self.ana.epochs(arranged=True)

        # get scores of baseline window
        if control:
            br = self.ana.score_fn(r=r)
        else:
            br = self.ana.score_fn(r=self.ana.epochs(baseline=True))

        # take averages of responses if not single-trial
        if not single_trial:
            r = equalize_groups(r, g, axis=1, reshape=True)
            r = np.nanmean(r, axis=-2)
        er = self.ana.score_fn(r=r)
        if single_trial:
            er = equalize_groups(er, g, axis=1, reshape=True)
            best = np.median(er, axis=-1).argmax(-1)
            #x[ zip(*enumerate( y.argmax(-1) )) ]
            er = er[zip(*enumerate(best))]

        if logged:
            p = 2 if squared else 1
            er = p * np.log(er)
            br = p * np.log(br)
        elif squared:
            np.power(er, 2, er)
            np.power(br, 2, br)

        # get quantiles of baseline score dist
        b_quantiles = nanpercentile(br, [25, 50, 75], axis=-1)
        b_med = b_quantiles[1]
        b_iqr = b_quantiles[2] - b_quantiles[0]
        # response score will either be the maximum averaged-ep score
        # or the response group with the highest median score
        if single_trial:
            e_quantiles = nanpercentile(er, [25, 50, 75], axis=-1)
            e_med = e_quantiles[1]
        else:
            e_med = er.max(-1)
        if estimate_trial_iqr:
            b_iqr = 0.5 * (b_iqr + e_quantiles[2] - e_quantiles[0])

        dprime = np.abs(e_med - b_med) / b_iqr
        if db:
            # db doesn't exactly make sense, but it makes the numbers
            # comparable with ESNR
            dprime = 10 * np.log10(dprime)
        return dprime

    def plot_esnr(self, single_trial=True, correct_bias=True, with_baseline=False, **kwargs):
        esnr = self.calc_esnr(single_trial=single_trial, correct_bias=correct_bias, **kwargs)
        if with_baseline:
            esnr_c = self.calc_esnr(use_baseline=True, single_trial=single_trial,
                                    correct_bias=correct_bias, **kwargs)
            fw = 9
            gspec = GridSpec(1, 2, width_ratios=[1, 2.5])
        else:
            # from proportions above: keep map width the same
            # and adjust box width to 50%
            map_w = 2.5 / 3.5 * 9
            box_w = (9 - map_w) * 0.5
            fw = map_w + box_w
            gspec = GridSpec(1, 2, width_ratios=[1, map_w / box_w])
        fig = plotters.plt.figure(figsize=(fw, 5))
        ax = fig.add_subplot(gspec[0])
        units = ' (dB)' if kwargs.get('db', True) else ''
        with plotters.sns.axes_style('whitegrid'):
            if with_baseline:
                pu.light_boxplot(
                    [esnr_c, esnr], ['Baseline', 'Response'], ['gray', 'k'], ax=ax
                )
            else:
                pu.light_boxplot([esnr], ['Response'], ['k'], ax=ax)
            ax.set_ylabel('ESNR' + units)
            ax.grid('on', axis='y')

        ax = fig.add_subplot(gspec[1])
        clim = tuple(np.percentile(esnr, [2, 98]))
        cmap = colormaps.nancmap('gray_r', 'skyblue')
        _, cbar = self.ana.dataset.chan_map.image(esnr, ax=ax, cmap=cmap, clim=clim, cbar=True, nan='')
        cbar.set_label('ESNR' + units)
        style = 'single' if single_trial else 'averaged'
        fig.suptitle(
            'ESNR ({0}) of {1} trials'.format(self.ana.scoring, style),
            fontsize=18
        )
        gspec.tight_layout(figure=fig, pad=0.1)
        fig.subplots_adjust(top=.90)  #, bottom=0.1)
        return fig

    def plot_dprime(self, **kwargs):
        single_trial = kwargs.get('single_trial', self.single_trial)
        dprime = self.calc_dprime(**kwargs)
        fig = plotters.plt.figure(figsize=(9, 5))
        gspec = GridSpec(1, 3)
        ax = fig.add_subplot(gspec.new_subplotspec((0, 0)))
        units = ' (dB)' if kwargs.get('db', True) else ''
        with plotters.sns.axes_style('whitegrid'):
            pu.light_boxplot([dprime], [''], ['k'], ax=ax)
            ax.set_ylabel('d-prime' + units)
            ax.grid('on', axis='y')

        ax = fig.add_subplot(gspec.new_subplotspec((0, 1), colspan=2))
        clim = tuple(np.percentile(dprime, [2, 98]))
        cmap = colormaps.nancmap('gray_r', 'skyblue')
        _, cbar = self.ana.dataset.chan_map.image(dprime, ax=ax, cmap=cmap, clim=clim, cbar=True, nan='')
        cbar.set_label('d-prime' + units)
        style = 'single' if single_trial else 'averaged'
        fig.suptitle(
            'd-prime ({0}) of {1} trials'.format(self.ana.scoring, style),
            fontsize=18
        )
        # fig.tight_layout()
        fig.subplots_adjust(top=.90)
        return fig

    def plot_array_roc(self, **kwargs):
        tp, fa, t = self.calc_array_roc(**kwargs)
        f, ax = subplots(figsize=(4.5, 4.5))
        plot_roc(
            (fa,), (tp,), colors=('#EB6567',),
            legend=True, lw=2, ax=ax
        )
        # f.tight_layout()
        return f

    def _esnr_map_fired(self):
        correct_bias = self.correct_esnr_bias
        # if bias is not corrected, then always plot the baseline over baseline
        with_baseline = not correct_bias
        if not self.single_trial:
            print('You have single trial set to False, but note that single trial is forced for ESNR')
        fig = self.plot_esnr(correct_bias=correct_bias, with_baseline=with_baseline, single_trial=True)
        # fig.canvas = None
        save_str = '{0}_ensr'.format(self.ana.scoring)
        return SavesFigure.live_fig(
            fig, sfile=save_str, spath=self.spath,
        )

    def _save_esnr_fired(self):
        esnr = self.calc_esnr(single_trial=True, correct_bias=True)
        rows, cols = self.dataset.chan_map.to_mat()
        from pyface.api import FileDialog, OK
        import os.path as op
        from scipy.io import savemat
        saver = FileDialog(action='save as', title='Save values (mat file)', wildcard='*.mat')
        if saver.open() == OK:
            fpath, ftype = op.splitext(saver.path)
            if not ftype:
                ftype = '.mat'
            fpath = fpath + ftype
            print('Saving bias-corrected ESNR to {}'.format(fpath))
            savemat(fpath, dict(esnr=esnr, electrode_rows=rows, electrode_cols=cols))


    def _dprime_map_fired(self):
        # not sure what defaults make sense yet
        fig = self.plot_dprime(
            single_trial=self.single_trial, db=True, logged=True
        )
        # fig.canvas = None
        save_str = '{0}_dprime'.format(self.ana.scoring)
        return SavesFigure.live_fig(
            fig, sfile=save_str, spath=self.spath,
        )

    def _array_roc_button_fired(self):
        fig = self.plot_array_roc(lags=self.lags)
        # fig.canvas = None
        save_str = 'array_roc'
        return SavesFigure.live_fig(fig, sfile=save_str, spath=self.spath)

    def default_traits_view(self):
        # do a single-entry hgroup to make it extendable
        esnr_group = VGroup(
            HGroup(Label('Correct bias'), UItem('correct_esnr_bias')),
            UItem('esnr_map'),
            UItem('save_esnr'),
            label='ESNR',
            show_border=True
        )
        rand_group = VGroup(
            UItem('dprime_map'),
            HGroup(Label('Single Trial'), UItem('single_trial')),
            HGroup(UItem('array_roc_button'), Label('Lags'), UItem('lags')),
        )

        param_handles = self._condition_list_factory()
        # condition value toggles group
        p_group_args = [Label('Fixed Values')] + list(param_handles.values())
        p_group_kwargs = dict(show_border=True, padding=0)
        p_group = VGroup(*p_group_args, **p_group_kwargs)

        traits_view = View(
            HGroup(
                VGroup(esnr_group, rand_group),
                VGroup(
                    HGroup(
                        VGroup(Label('Map Condition'),
                               UItem('conditions', style='simple'),
                               show_border=True, padding=0),
                        p_group
                    ),
                    UItem('mag_maps'),
                    UItem('blend_map'),
                    UItem('energy_map'),
                    label='condition maps'
                )
            ),
            resizable=True, title='.name'
        )
        return traits_view


class FrequencyAnalysis(AnalysisModule):
    """
    This module computes power maps and ratios in the frequency domain.

    """

    name = 'Spectral Power'

    plot_spectra_b = Button('Plot spectra')

    bandwidth = Float(15.0)
    _nw = Property(Float)

    p_lo = Float(0.0)
    p_hi = Float(-1.0)

    time_average = Bool(False)
    sub_average = Bool(False)
    modulated_spectra = Bool(True)

    false_epoch_offset = Float(-0.4)
    synced_traits = AnalysisModule.synced_traits + ('false_epoch_offset',)

    def __init__(self, *args, **kwargs):
        super(FrequencyAnalysis, self).__init__(*args, **kwargs)
        self._spectra_cache = dict()

    @property_depends_on('bandwidth, ana.epoch_start, ana.epoch_end')
    def _get__nw(self):
        T = np.sum(self.ana._epoch_window(samps=False))
        return T * self.bandwidth / 2.0

    def _get_spectra(self):
        # these uniquely (?) specify the spectra to be computed
        key = (
            self.bandwidth, self.false_epoch_offset,
            self.epoch_start, self.epoch_end, self.time_average,
            self.ana.epoch_filter
        )

        if key in self._spectra_cache:
            return self._spectra_cache[key]

        if self.time_average:
            # treat experimental and baseline data the same?
            # i.e. average both in the time domain?
            epochs = self.ana.ep_average(reshape=False)
            f_epochs = self.ana.ep_average(baseline=True, reshape=False)
        else:
            epochs, groups = self.ana.epochs(arranged=True, mask_outliers=True)
            f_epochs = self.ana.epochs(baseline=True, mask_outliers=True)
            if self.sub_average:
                groups.append(np.sum(groups))
                for n in range(len(groups) - 1):
                    sl = (slice(None), slice(groups[n], groups[n + 1]))
                    ep_ = epochs[sl]
                    ep_ -= ep_.mean(1)[:, None, :]
                    # this should bias the variance in an eaual fashion
                    ep_ = f_epochs[sl]
                    ep_ -= ep_.mean(1)[:, None, :]

        fx, pss, _ = multi_taper_psd(
            epochs, BW=self.bandwidth, Fs=self.dataset.Fs,
            jackknife=False, adaptive=False
        )

        fx, pnn, _ = multi_taper_psd(
            f_epochs, BW=self.bandwidth, Fs=self.dataset.Fs,
            jackknife=False, adaptive=False
        )

        self._spectra_cache[key] = fx, pss, pnn
        return self._get_spectra()

    # XXX: can probably be greatly simplified with "equalize_groups",
    # either in this method or in _get_spectra()
    def _filled_conditions(self):
        # returns a nan-filled array of spectra that can
        # be reshaped by rolled_conditions_shape + (n_rep,)
        fx, pss, pnn = self._get_spectra()
        c_shape = self.exp.rolled_conditions_shape()
        if self.time_average:
            # can already be reshaped by rolled_conditions_shape
            pss = pss.reshape((len(pss),) + c_shape + (len(fx),))
            pnn = pnn.reshape(pss.shape)
            return fx, pss, pnn
        conds, _ = self.exp.enumerate_conditions()
        uconds = np.unique(conds)
        groups = [np.sum(conds == i) for i in uconds]
        mx_len = max(groups)
        filled_spectra = np.ones((len(pss), len(uconds), mx_len, len(fx)))
        filled_spectra.fill(np.nan)
        filled_baseline = filled_spectra.copy()
        cgroups = np.r_[0, np.cumsum(groups)]
        for i in range(len(uconds)):
            filled_spectra[:, i, :groups[i]] = pss[:, cgroups[i]:cgroups[i + 1]]
            filled_baseline[:, i, :groups[i]] = pnn[:, cgroups[i]:cgroups[i + 1]]
        filled_spectra.shape = (len(pss),) + c_shape + (mx_len, len(fx))
        filled_baseline.shape = (len(pss),) + c_shape + (mx_len, len(fx))
        return fx, filled_spectra, filled_baseline

    def _plot_spectra_b_fired(self, **traits):
        fig = self.plot_spectra()
        # steal title from fig axes
        title = fig.axes[0].get_title()
        save_str = title.replace(':', '').replace(' ', '_').replace('.', '_').replace('\n', '')
        # fig.canvas = None
        return SavesFigure.live_fig(
            fig, sfile=save_str, spath=self.spath, **traits
        )

    def plot_spectra(self, tabs=()):
        """
        Operates in two modes:

        * if no channel is selected in the Analysis, then plot all
              channels at the presently chosen combination of conditions

        * if a channel is selected, then plots variations along the
              chosen (checked) condition values, holding others constant

        """
        fx, pss, pnn = self._filled_conditions()
        if not self.time_average:
            # average the trial dimension here
            pss_sig = np.nanstd(np.log(pss), axis=-2)
            pss_sig /= np.sqrt(pss.shape[-2])
            pss = np.nanmean(np.log(pss), axis=-2)
            pss_err_lo, pss_err_hi = map(np.exp, (pss - pss_sig, pss + pss_sig))
            pss = np.exp(pss)

            pnn_sig = np.nanstd(np.log(pnn), axis=-2)
            pnn_sig /= np.sqrt(pnn.shape[-2])
            pnn = np.nanmean(np.log(pnn), axis=-2)
            pnn_err_lo, pnn_err_hi = map(np.exp, (pnn - pnn_sig, pnn + pnn_sig))
            pnn = np.exp(pnn)

        if self.ana.selected_site < 0:
            tabs = ()
            title = 'All chan ' + self.current_conditions()
            cm = self.ana._colors['allchan']
            colors = cm(np.linspace(0, 1, len(self.ana.dataset)))
        else:
            if not tabs:
                tabs = self._condition_groups
            if len(tabs) != 1:
                raise ValueError('Can only plot variation in one condition')

            title = 'Chan {0} '.format(self.ana.selected_site)
            title = title + self.current_conditions(floating=tabs)
            cm = self.ana._colors[tabs[0]]
            colors = cm(np.linspace(0, 1, len(self.values(tabs[0]))))

        pss = self.slice_for_rolled_average(resp_avg=pss, tabs=())
        pnn = self.slice_for_rolled_average(resp_avg=pnn, tabs=())
        if not self.time_average:
            pss_err_lo = self.slice_for_rolled_average(
                resp_avg=pss_err_lo, tabs=()
            )
            pss_err_hi = self.slice_for_rolled_average(
                resp_avg=pss_err_hi, tabs=()
            )
            pnn_err_lo = self.slice_for_rolled_average(
                resp_avg=pnn_err_lo, tabs=()
            )
            pnn_err_hi = self.slice_for_rolled_average(
                resp_avg=pnn_err_hi, tabs=()
            )
        assert pss.ndim == 2
        assert pnn.ndim == 2
        p_hi = self.p_hi if self.p_hi >= 0 else fx[-1]
        xl = (0, p_hi)
        fig = plotters.plt.figure()
        ax = fig.add_subplot(111)
        if self.time_average:
            if self.modulated_spectra:
                for s_spec, n_spec, c in zip(pss, pnn, colors):
                    spec = 10 * (np.log10(s_spec) - np.log10(n_spec))
                    ax.plot(fx, spec, color=c)
                ax.set_title(title)
                ax.set_xlim(xl)
                ax.set_ylabel('dB')
            else:
                for s_spec, c in zip(pss, colors):
                    ax.semilogy(fx, s_spec, color=c)
                ax.semilogy(fx, np.exp(np.log(pnn).mean(0)), color='k')
                ax.set_title(title)
                ax.set_xlim(xl)
                ax.set_ylabel('PSD')
            return fig
        if self.modulated_spectra:
            nrm = np.log10(pnn).mean(0)
            table_spectra = 10 * (np.log10(pss) - nrm)
            # table_spectra = 10 * np.log10(table_spectra)
            table_err_lo = 10 * (np.log10(pss_err_lo) - nrm)
            table_err_hi = 10 * (np.log10(pss_err_hi) - nrm)
            table_sigma = np.array(
                [table_err_lo, table_err_hi]
            ).transpose(1, 0, 2)
            # fig = figure()
            # ax = fig.add_subplot(111)
            # ax.set_color_cycle(colors)
            for n, c in enumerate(colors):
                nrm = np.log10(pnn[n])
                spec = 10 * (np.log10(pss[n]) - nrm)
                err_lo = 10 * (np.log10(pss_err_lo[n]) - nrm)
                err_hi = 10 * (np.log10(pss_err_hi[n]) - nrm)
                pu.filled_interval(
                    ax.plot, fx, spec, (err_lo, err_hi), c, ax=ax
                )
            # for spec, sig, c in zip(table_spectra, table_sigma, colors):
            #     pu.filled_interval(ax.plot, fx, spec, sig, c, ax=ax)
            # ax.plot(fx, table_spectra.T, lw=.4)
            ax.set_title(title)
            ax.set_xlim(xl)
            # ax.set_ylim(-20, 20)
            ax.set_ylabel('dB')
        else:
            table_spectra = pss
            table_sigma = np.array(
                [pss_err_lo, pss_err_hi]
            ).transpose(1, 0, 2)
            # fig = figure()
            # ax = fig.add_subplot(111)
            # ax.set_color_cycle(colors)
            # for spec, sig, c in zip(pss, pss_sig, colors):
            for spec, sig, c in zip(table_spectra, table_sigma, colors):
                pu.filled_interval(ax.semilogy, fx, spec, sig, c, ax=ax)
            # averaging all the average noise spectra for each condition??
            pnn = np.exp(np.log(pnn).mean(0))
            pnn_err_lo = np.exp(np.log(pnn_err_lo).mean(0))
            pnn_err_hi = np.exp(np.log(pnn_err_hi).mean(0))
            pu.filled_interval(
                ax.semilogy, fx, pnn, (pnn_err_lo, pnn_err_hi), 'k'
            )
            # ax.semilogy(fx, pss.T, lw=.4)
            # ax.semilogy(fx, pnn.T, 'k', lw=.4)
            ax.set_title(title)
            ax.set_xlim(xl)
            lims = np.percentile(
                np.log10(pss[..., fx < p_hi]), [5, 99]
            )
            y_mn = np.floor(min(lims))
            y_mx = np.ceil(max(lims))
            ax.set_ylim(10 ** y_mn, 10 ** y_mx)

        return fig

    def default_traits_view(self):
        c_handles = self._condition_list_factory()
        c_group = list(c_handles.values())
        c_group.append(
            Item('_condition_groups', style='custom', label='Variable Cond.')
        )

        traits_view = View(
            HGroup(
                VGroup(
                    Item('epoch_start', label='start', width=4),
                    Item('epoch_end', label='stop', width=4),
                    Item(
                        'false_epoch_offset',
                        label='Baseline offset', width=4
                    )
                ),
                VGroup(
                    Item('bandwidth', label='freq BW', width=4),
                    Item('_nw', label='NW', style='readonly', width=4),
                    Item('p_lo', label='Band (low)', width=4),
                    Item('p_hi', label='Band (high)', width=4)
                ),
                VGroup(*c_group),
                VGroup(
                    Item('time_average', label='avg in time'),
                    Item('sub_average', label='Subtract Avg'),
                    Item('modulated_spectra', label='plot modulated'),
                    UItem('plot_spectra_b')
                )
            ),
            resizable=True, title='.name'
        )
        return traits_view


class Spectrogram(FrequencyAnalysis):
    name = 'Spectrogram'

    plot_spectra_b = Button('Plot')

    lag = Float(0.01)
    strip = Float(0.05)

    detrend = Bool(False)
    adaptive = Bool(False)
    over_samp = Float(2.0)

    high_res = Bool(False)
    use_scipy = Bool(False)

    t_inches = Float(1.0)
    f_t_ratio = Float(1.25)

    zx = Bool(True)

    colormap = 'Spectral_r'

    @property_depends_on('bandwidth, strip')
    def _get__nw(self):
        return self.strip * self.bandwidth / 2.0

    def make_spectrogram(self):
        # xxx: this is copied from ChannelAnalysis.channel_plot --
        # would consider pattern-ifying
        Fs = self.dataset.Fs
        i, j = self.dataset.chan_map.subset([self.selected_site]).to_mat()
        exp = self.exp
        fixed_values = [(param, self.value(param))
                        for param in exp.enum_tables]
        epochs, _ = self.ana.epochs(
            chan=self.selected_site, arranged=fixed_values, mask_outliers=True
        )
        epochs = epochs.squeeze()

        lag = int(Fs * self.lag)
        strip = int(Fs * self.strip)
        nfft = nextpow2(strip)
        pl = 1.0 - float(lag) / strip

        common_kwargs = dict(
            detrend='linear' if self.detrend else '',
            Fs=Fs, adaptive=self.adaptive, NFFT=nfft, NW=self._nw,
            pl=pl
        )

        if self.high_res:
            tx, fx, ptf = mtm_spectrogram(
                epochs, strip, samp_factor=self.over_samp, low_bias=.95,
                **common_kwargs
            )
            # dt = tx[1] - tx[0]
            # n_cut = int(.01 / dt)
            # Maybe this is better hard-coded at ~ 4
            n_cut = 4
            tx = tx[n_cut:-n_cut]
            ptf = ptf[..., n_cut:-n_cut].copy()
        elif self.use_scipy:
            # unwrap common kwargs
            common_kwargs['noverlap'] = int(strip * common_kwargs.pop('pl'))
            common_kwargs['nfft'] = common_kwargs.pop('NFFT')
            common_kwargs['fs'] = common_kwargs.pop('Fs')
            common_kwargs.pop('adaptive')
            common_kwargs.pop('NW')
            common_kwargs['nperseg'] = strip
            from scipy.signal import spectrogram
            fx, tx, ptf = spectrogram(epochs, **common_kwargs)
        else:
            tx, fx, ptf = mtm_spectrogram_basic(
                epochs, strip, **common_kwargs
            )
        tx += self.epoch_start

        if self.zx:
            m = tx < 0
            if self.high_res:
                m
            ptf = np.log(ptf)
            mu = ptf[..., m].mean(-1).mean(0)
            sig = ptf[..., m].var(0).mean(-1) ** 0.5
            ptf = (ptf.mean(0) - mu[:, None]) / sig[:, None]

        return tx, fx, ptf

    def plot_spectra(self):
        tx, fx, ptf = self.make_spectrogram()
        tx *= 1000
        if not self.zx:
            ptf = np.log(ptf).mean(0)

        f_min = 0 if self.p_lo < 0 else fx.searchsorted(self.p_lo)
        f_max = -1 if self.p_hi < 0 else fx.searchsorted(self.p_hi)
        ptf = ptf[f_min:f_max]
        fx = fx[f_min:f_max]

        # fw = tx.ptp() * 1.25 / 100 + .25
        # fh = fx.ptp() * 0.5 / 100
        # t inches controls inches of figure per 100 ms
        fw = tx.ptp() * self.t_inches / 100
        # f_t_ratio controls the relative pitch of the frequency axis:
        # so "n" inches per 100 ms will be n * f_t_ratio inches per 100 Hz
        fh = fx.ptp() * self.t_inches * self.f_t_ratio / 100
        print(fw, fh)
        fig = plotters.plt.figure(figsize=(fw + .25, fh))
        ax = fig.add_subplot(111)
        ax.imshow(
            ptf, extent=[tx[0], tx[-1], fx[0], fx[-1]], cmap=self.colormap,
            origin='lower'
        )
        ax.axis('auto')
        ax.set_xlim(tx[0], tx[-1])
        ax.set_ylim(fx[0], fx[-1])
        ax.set_xlabel('ms')
        ax.set_ylabel('Hz')

        title = 'Chan {0}\n'.format(self.ana.selected_site)
        title = title + self.current_conditions()
        ax.set_title(title, fontsize=10)

        cbar = fig.colorbar(ax.images[0], ax=ax, shrink=.5)
        cbar.locator = ticker.MaxNLocator(nbins=3)
        title = 'Z score' if self.zx else 'Log-power'
        cbar.set_label(title)
        # fig.subplots_adjust(left=.1, right=.95, bottom=.125, top=0.9)
        # fig.tight_layout()
        return fig

    def _plot_spectra_b_fired(self):
        return super(Spectrogram, self)._plot_spectra_b_fired()

    def default_traits_view(self):
        c_handles = self._condition_list_factory()
        c_group = list(c_handles.values())

        traits_view = View(
            HGroup(
                VGroup(
                    Item('epoch_start', label='start', width=4),
                    Item('epoch_end', label='stop', width=4),
                    Item('strip', label='Strip', width=4),
                    Item('lag', label='Lag', width=4),
                    Item('over_samp', label='Over-samp', width=4)
                ),
                VGroup(
                    Item('bandwidth', label='freq BW', width=4),
                    Item('_nw', label='NW', style='readonly', width=4),
                    Item('p_lo', label='Band (low)', width=4),
                    Item('p_hi', label='Bands (high)', width=4)
                ),
                VGroup(*(c_group +
                         [Item('t_inches', label='Fig wid', width=4),
                          Item('f_t_ratio', label='H/W ratio', width=4)])),
                VGroup(
                    Item('detrend', label='De-trend'),
                    Item('zx', label='Z-xform'),
                    Item('high_res', label='High time res.'),
                    Item('use_scipy', label='Use Scipy'),
                    UItem('plot_spectra_b')
                )
            ),
            resizable=True, title='.name'
        )
        return traits_view


class StimDecoding(AnalysisModule, Decoder):
    """
    Perform decoding of stim conditions based on the responses.
    """

    name = 'Stim Decoding'
    rms_lag_features = Bool(False)
    avg_reg = Bool(False)
    avg_ref = Bool(False)

    classifier = Enum(
        'svdlda', ('svdlda', 'svdksvc', 'svdlsvc', 'svdlogres', 'svdgnb')
    )
    n_comps = Enum(0.99, (0.9, 0.95, 0.98, 0.99, 0.999, 'auto', 'entry'))
    _n_comps_manual = Int
    n_folds = Int(6)
    decode = Button('Decode Summary')
    plot_components = Bool(False)
    keys = List([])
    pop_key = Button('Pop')
    show_key = Button('Show')
    key = Tuple
    only_hit = Bool(False)
    only_missed = Bool(False)
    error_tol = Int(0)

    def __init__(self, *args, **kwargs):
        self._roc_cache = dict()
        super(StimDecoding, self).__init__(*args, **kwargs)
        self._full_exp = self.exp[:]

    @on_trait_change('only_hit, only_missed')
    def _toggle_epochs(self):
        # if both are turned off, then revert to the full experiment
        if not (self.only_hit or self.only_missed):
            self.exp = self._full_exp
            return
        # incompatible mode -- force a resolution
        if self.only_hit and self.only_missed:
            self.trait_setq(only_hit=False, only_missed=False)
            self._toggle_epochs()
            return
        hits = self._get_hits_misses()
        if self.only_hit:
            self.exp = self._full_exp.subexp(hits)
        else:
            self.exp = self._full_exp.subexp(~hits)

    def _get_hits_misses(self):
        # make this private-ish for now, since it relies on quite a few
        # assumptions as well as internal state
        conditions = list()
        for c in self.exp.enum_tables:
            if c in self._condition_groups:
                continue
            else:
                conditions.append((c, getattr(self, c + '_value')))
        # Indexing epochs in this order gives sorted conditions a la
        # the "arranged" epochs of the feature vector. Now the job is to
        # put hits and misses back into the original epoch order.
        epoch_order = list(ordered_epochs(self._full_exp, conditions))
        rev_order = [epoch_order.index(n) for n in range(len(epoch_order))]

        roc = self.cache_roc()
        pred = roc.cls_score.argmax(1)
        true = roc.cv_gen.y
        hits = np.abs(pred - true) <= self.error_tol
        hits = hits[rev_order]
        return hits

    def _generate_key(self):
        nc = self.n_comps if self.n_comps != 'entry' else self._n_comps_manual
        k = ('{0:.5f}'.format(self.ana.peak_min),
             '{0:.5f}'.format(self.ana.peak_max),
             nc, str(self.classifier),
             self.avg_ref, self.avg_reg, self.rms_lag_features)
        tfp = self._tf_props()
        k = k + tuple(tfp.items())
        conditions = dict()
        # hack this info into the key as well
        for c in self.exp.enum_tables:
            if c in self._condition_groups:
                conditions[c] = 'all'
            else:
                conditions[c] = getattr(self, c + '_value')

        return k + tuple(conditions.values())

    def cache_roc(self, key=None, roc=None):
        # keys are:
        # * response window (peak_min, peak_max)
        # * # components in SVD compression
        # * time-freq properties
        # * classifier method
        if key is None:
            key = self.key
        if roc is not None:
            if not key in self.keys:
                self.keys.append(key)
                self.key = key
            self._roc_cache[key] = roc
            return
        return self._roc_cache[key]

    def get_samples(self, conditions, **kwargs):
        """
        Prepare the samples of the feature vector.

        conditions:
          A dictionary of labels for each condition. The value for
          each condition label can be 'all' (meaning provide samples
          for all values of this condition) or it can be a specific
          value (meaning return samples with this value held constant).
        kwargs:
          Options for decoding.prepare_sampling()
        """

        float_conditions = [k for k, v in conditions.items() if v == 'all']
        float_labels = [self.labels(k) for k in float_conditions]
        if len(float_labels) > 1:
            class_labels = [', '.join(p) for p in product(*float_labels)]
        else:
            class_labels = float_labels[0][:]

        if self.rms_lag_features:
            fixed_conditions = [(k, v)
                                for k, v in conditions.items() if v != 'all']
            epochs, g = self.ana.epochs(arranged=fixed_conditions,
                                        subresponse=True)
            classes = np.concatenate(
                [np.repeat(i, g_) for i, g_ in enumerate(g)]
            )
            nchan, nsamp = epochs.shape[:2]
            features = np.empty((nsamp, 2 * nchan))
            for i in range(nsamp):
                resp = epochs[:, i]
                rms = resp.std(-1)
                lag, _ = peak_lags(resp, interp=4)
                features[i] = np.r_[rms, lag]

            chan_mask = np.ones((nchan,), dtype='?')
            r = features, classes, class_labels, chan_mask
        else:
            kwargs.setdefault('average_regression', self.avg_reg)
            kwargs.setdefault('average_reference', self.avg_ref)
            r = prepare_samples(self.ana, conditions, **kwargs)
        return r

    def _decode_fired(self, key=None):
        if not key:
            key = self._generate_key()
        try:
            roc = self.cache_roc(key=key)
        except KeyError:
            conditions = dict()
            for c in self.exp.enum_tables:
                if c in self._condition_groups:
                    conditions[c] = 'all'
                else:
                    conditions[c] = getattr(self, c + '_value')
            r = self.get_samples(conditions)
            samples, classes, class_labels, chan_mask = r
            if self.n_comps == 'auto':
                do_plot = self.plot_components
                r = self.train_compression_step(
                    samples, classes, plot=do_plot
                )
                if do_plot:
                    comps, f = r
                    # f.canvas = None
                    save_str = 'CV_component_accuracy'
                    foo1 = SavesFigure.live_fig(
                        f, sfile=save_str, spath=self.spath
                    )
                else:
                    comps = r
            elif self.n_comps == 'entry':
                comps = self._n_comps_manual
            else:
                comps = self.n_comps

            roc = self.kfold_decoding(samples, classes, n_components=comps)
            roc.class_labels = class_labels
            roc.chan_mask = chan_mask
            self.cache_roc(roc=roc, key=self._generate_key())
        f = self.plot_decoding_summary(roc)
        # f.canvas = None
        foo = SavesFigure.live_fig(
            f, sfile='decoding_summary', spath=self.spath
        )
        return foo

    def _pop_key_fired(self):
        if len(self.keys):
            self._roc_cache.pop(self.key)
            self.keys.remove(self.key)
            self.trait_setq(key=self.keys[0] if len(self.keys) else ())

    def _show_key_fired(self):
        self._decode_fired(key=self.key)

    def plot_decoding_summary(self, roc):
        results = self.unpack_decode(roc)
        cm = results.confusion
        error = results.error
        chance = results.chance_error
        auc = results.auc_matrix

        f, axs = subplots(2, 2, figsize=(10, 10))
        confusion_matrix_plot(cm, '', roc.class_labels, ax=axs[0, 0])
        auc_matrix_plot(auc, '', roc.class_labels, ax=axs[1, 0])
        with rc_context({'legend.fontsize': '10'}):
            plot_roc_bunch(roc, cls_labels=roc.class_labels, ax=axs[0, 1])

        t = 'Decode error: {0:.2f} units (chance {1:.2f})'
        axs[0, 1].set_title(t.format(error, chance))
        axs[1, 1].axis('off')
        # f.tight_layout(pad=0)
        return f

    def default_traits_view(self):
        c_handles = self._condition_list_factory()
        c_group = list(c_handles.values())
        c_group.extend(
            [Item('_condition_groups', style='custom', label='Variable Cond.'),
             Item('rms_lag_features', label='RMS & Lag'),
             HGroup(VGroup(Label('Com Avg Ref'), UItem('avg_ref')),
                    VGroup(Label('Com Avg Reg'), UItem('avg_reg')))]
        )
        traits_view = View(
            HGroup(
                VGroup(*c_group),
                VGroup(
                    Item('n_comps', label='Components'),
                    Item('_n_comps_manual', label='Components',
                         visible_when='n_comps == "entry"'),
                    Item('n_folds', label='CV Folds'),
                    Item('classifier', label='Method'),
                    Item('decode'),
                    Item('plot_components', label='Plot Comps',
                         enabled_when='n_comps=="auto"')
                ),
                VGroup(
                    HGroup(
                        Item('key', editor=EnumEditor(name='object.keys')),
                        UItem('pop_key'), UItem('show_key')
                    ),
                    HGroup(
                        VGroup(
                            Item('only_hit', label='Correct trials',
                                 enabled_when='only_missed==False'),
                            Item('only_missed', label='Mislabeled trials',
                                 enabled_when='only_hit==False')
                        ),
                        Item('error_tol', label='tol')
                    )

                )

            ),
            resizable=True, title='.name'
        )
        return traits_view


class ToposChronos(AnalysisModule):
    """
    Uses SVD to calculate Topos (spatial) and Chronos (temporal) modes
    of the Analysis window.
    """

    name = 'Topos & Chronos'

    _mx_sv = Int(1)
    _mn_sv = Int(0)
    mode = Range(low='_mn_sv', high='_mx_sv')

    grab_focus = Button('Plot mode')
