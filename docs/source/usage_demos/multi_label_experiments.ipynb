{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Multi-label experiments\n",
    "\n",
    "All analysis modules adapt to the `StimulatedExperiment` object of a dataset (the `.exp` attribute) to organize trials by condition label. The single-label case is the most straightforward. With two or more labels, modules change their function based on the status of condition labels. **This notebook demonstrates how modules response to experiments with multi-label conditions.** The simplified model experiments are mapping orientation preference and retinotopy in primary visual cortex."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from itertools import product"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ecogdata.util import Bunch, equalize_groups\n",
    "from ecogdata.channel_map import ChannelMap\n",
    "from ecogdata.expconfig.exp_descr import StimulatedExperiment\n",
    "from ecogtopus.anatool import Analysis\n",
    "from ecogtopus.anatool.modules import ChannelAnalysis, AverageAnalysis, ArrayMaps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Two labels\n",
    "\n",
    "Nested or hierarchical condition levels are a common experiment structure. Imagine mapping primary visual cortex orientation preference with gratings rotated at $N$ angles. Then for each angle, imagine mapping perceptual sensitivity using different contrast levels. So each visual grating event can bee given two labels: angle and contrast. For simulation, we'll imagine that there is a broad spatial mapping for orientation (instead of pinwheels within a V1 column)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 7 row 11 column electrode array\n",
    "channel_map = ChannelMap(range(7 * 11), (7, 11))\n",
    "channels = 7 * 11\n",
    "\n",
    "# make 5 * 4 * 20 \"trials\" with 0.5 seconds on/off at 200 samps/sec\n",
    "fs = 200\n",
    "trials = 20\n",
    "\n",
    "# Five levels of \"condition A\" (e.g. visual grating angle in 30 deg steps)\n",
    "angles = np.arange(6) * 30\n",
    "\n",
    "# Four levels of \"condition B\" (e.g. grating contrast in 25% steps)\n",
    "contrasts = np.arange(1, 5) * 25\n",
    "\n",
    "# Use \"product\" to create all possible pairs of angles & contrasts\n",
    "angle_contrast_pairs = np.array(list(product(angles, contrasts)))\n",
    "\n",
    "trial_time = 1 * fs\n",
    "# A timestamp each second offset by 1.25 seconds\n",
    "timestamps = np.arange(1, trials * len(angles) * len(contrasts) + 1) * fs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `StimulatedExperiment` is constructed from a class method that takes the basic condition sequence and repeats it as many times as needed to equal the length of the timestamps. The `condition_order` is loosely hierarchical (some visualizations default to the first listed label)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp = StimulatedExperiment.from_repeating_sequences(\n",
    "    timestamps, \n",
    "    dict(angle=angle_contrast_pairs[:, 0], contrast=angle_contrast_pairs[:, 1]),                                                \n",
    "    condition_order=('angle', 'contrast'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now make up event responses that march across the channels left to right with distance-weighted strength."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coords = np.c_[channel_map.to_mat()]\n",
    "response_duration = int(0.25 * fs)\n",
    "response_snr = {25: 2, 50: 3, 75: 5, 100: 7}\n",
    "maps = {}\n",
    "\n",
    "# For sake of simulation, say that angles have a \"place code\"\n",
    "for i, a in enumerate(angles):\n",
    "    # centered on 3rd row, and a leftward moving column\n",
    "    center_location = np.array([3, (i + 0.5) * 2])\n",
    "    # inverse distance weight (attenuation strength k = 1.5)\n",
    "    k = 1.5\n",
    "    dist = np.linalg.norm(coords - center_location, axis=1)\n",
    "    distance_weight = (1 + dist * k) ** -1\n",
    "    maps[a] = distance_weight\n",
    "\n",
    "data = np.random.randn(channels, (trials * len(angle_contrast_pairs) + 2) * trial_time)\n",
    "for i, t in enumerate(exp.time_stamps):\n",
    "    a = exp.angle[i]\n",
    "    c = exp.contrast[i]\n",
    "    d_weight = maps[a]\n",
    "    snr = response_snr[c]\n",
    "    response = np.sin(np.linspace(0, np.pi, response_duration))\n",
    "    response = response / np.std(response) * d_weight[:, None]\n",
    "    data[:, t:t + response_duration] += (snr - 1) * response"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f, axs = plt.subplots(1, len(angles), figsize=(10, 3))\n",
    "for i in range(len(angles)):\n",
    "    im = axs[i].imshow(channel_map.embed(maps[angles[i]]), origin='upper')\n",
    "    axs[i].axis('off')\n",
    "    axs[i].set_title('Angle {}'.format(angles[i]))\n",
    "_ = f.suptitle('Localized response weights', fontsize=16)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset = Bunch(name='Dummy dataset',\n",
    "                chan_map=channel_map,\n",
    "                data=data,\n",
    "                Fs=fs,\n",
    "                exp=exp,\n",
    "                units='uV')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_module(ana, cls):\n",
    "    return [m for m in ana.modules if isinstance(m, cls)][0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Constructing an Analysis with bells & whistles\n",
    "\n",
    "Here I'm demonstrating more control over plot asthetics and condition labeling."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Over-ride default labels with units\n",
    "labels = dict()\n",
    "labels['angle'] = ['{:d} deg'.format(int(a)) for a in angles]\n",
    "labels['contrast'] = ['{:d}%'.format(int(c)) for c in contrasts]\n",
    "\n",
    "# Set different color codes for each label\n",
    "colors = dict()\n",
    "colors['angle'] = 'rainbow'\n",
    "colors['contrast'] = 'viridis'\n",
    "\n",
    "ana = Analysis(dataset, \n",
    "               false_epoch_offset=-0.2, \n",
    "               peak_min=0,\n",
    "               peak_max=0.2,\n",
    "               epoch_end=0.4,\n",
    "               scoring='ptp',\n",
    "               condition_labels=labels,\n",
    "               condition_colors=colors,\n",
    "               channel_colors='Blues',  # channel color code for all-channel plot\n",
    "               modules=[ChannelAnalysis, AverageAnalysis, ArrayMaps])\n",
    "ana._attach_ep_plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** *currently the base `Analysis` object is only sensitive to the first listed condition label: in this case the response map shows the best angle per site. Modules are more function at this stage.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana._attach_array_plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Grand average of each channel."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.ep_plot.fig"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Color-mapped averages for site 3, 9. Note that there are multiple traces at each angle (color). This is due to multiple contrasts per angle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.selected_site = channel_map.lookup(3, 9)\n",
    "ana.ep_plot.fig"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Channel analysis with two labels\n",
    "\n",
    "With two or more condition labels, different functions organize trials with additional rules. Condition values can be **\"floating\"** or **\"fixed\"**. For example, plot all trials for the range of angles (floating) at full contrast (fixed)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "channel_ana = get_module(ana, ChannelAnalysis)\n",
    "channel_ana.set_value('contrast', 100)\n",
    "f = channel_ana.channel_plot('angle')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now switch to floating contrast and angle fixed at 120"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "channel_ana.set_value('angle', 120)\n",
    "f = channel_ana.channel_plot('contrast')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tuning curves are drawn over the floating condition (defines the x-axis). There can also be a \"repeated\" condition, such that multiple curves are drawn while cycling through these condition levels. In the 2-label case, the tuning condition always determines the repeated condition. See below for 3 or more labels.\n",
    "\n",
    "Here I set the tuning condition to angle, which will repeat over contrast."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.scoring = 'ptp'\n",
    "ana.peak_min = 0.0\n",
    "ana.peak_max = 0.2\n",
    "f = channel_ana.plot_tuning_curve('angle', repeat_for='contrast')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And vice-versa -- note that the condition colors respect the colors I specified at the outset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = channel_ana.plot_tuning_curve('contrast', repeat_for='angle')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And it is possible to not repeat, as well"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "channel_ana.set_value('contrast', 25)\n",
    "f = channel_ana.plot_tuning_curve('angle')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evoked potential averages with two labels"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avg_ana = get_module(ana, AverageAnalysis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With multiple labels, the `AverageAnalysis` can plot a table of responses for any site, with rows and columns specified in order."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = avg_ana.plot_site_table(tabs=('angle', 'contrast'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Reversing the rows/columns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = avg_ana.plot_site_table(tabs=('contrast', 'angle'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another site with different activation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.selected_site = 37\n",
    "fig = avg_ana.plot_site_table(tabs=('contrast', 'angle'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Array-wide averages can be shown for a fixed set of conditions--the response will be drawn from the current settings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avg_ana.set_value('angle', 60)\n",
    "avg_ana.set_value('contrast', 75)\n",
    "avg_ana.current_conditions()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avg_ana.y_err = 'stdev'\n",
    "avg_ana.fix_scale = False  # if fix_scale is True, then the y-limits are consistent for all channels / conditions\n",
    "f = avg_ana.plot_array_avg()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A different contrast level"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avg_ana.set_value('contrast', 25)\n",
    "f = avg_ana.plot_array_avg()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Array maps with two labels\n",
    "\n",
    "Array maps also apply the floating / fixed condition concept."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "array_maps = get_module(ana, ArrayMaps)\n",
    "array_maps.single_trial = True\n",
    "array_maps.set_value('contrast', 100)\n",
    "f = array_maps.plot_maps('angle')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "array_maps.set_value('angle', 60)\n",
    "f = array_maps.plot_maps('contrast')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "ESNR is computed on the highest scoring condition level, so it is not sensitive to floating or fixed conditions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = array_maps.plot_esnr()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3 or more labels\n",
    "\n",
    "Imagine a two dimensional visual stimulation. For example, the V1 retinotopic transformation *roughly* codes radial coordinates in visual field to cartesian coordinates on the cortical surface. An experiment might map this retinotopy with multiple contrast levels for sensitivity. So each stimulus variation has 3 labels: angle, radius, contrast."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 7 row 11 column electrode array\n",
    "channel_map = ChannelMap(range(7 * 11), (7, 11))\n",
    "channels = 7 * 11\n",
    "fs = 200\n",
    "trials = 20\n",
    "\n",
    "# Six levels of angles 0 - 150 degrees\n",
    "angles = np.arange(6) * 30\n",
    "\n",
    "# Four concentricity levels\n",
    "radii = np.arange(4) * 5 + 2.5\n",
    "\n",
    "# Four levels of grating contrast in 25% steps\n",
    "contrasts = np.arange(1, 5) * 25\n",
    "\n",
    "# All possible combinations\n",
    "condition_labels = np.array(list(product(angles, radii, contrasts)))\n",
    "\n",
    "trial_time = 1 * fs\n",
    "# A timestamp each second offset by 1.25 seconds\n",
    "timestamps = np.arange(1, trials * len(condition_labels) + 1) * fs\n",
    "exp = StimulatedExperiment.from_repeating_sequences(\n",
    "    timestamps, \n",
    "    dict(angle=condition_labels[:, 0], \n",
    "         radius=condition_labels[:, 1],\n",
    "         contrast=condition_labels[:, 2]),\n",
    "    condition_order=('angle', 'radius', 'contrast'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make up a dataset with radial-to-cartesian response centers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# angle --> left-right on array\n",
    "# radius --> up-down on array\n",
    "angle_x_loc = dict()\n",
    "for i, a in enumerate(angles):\n",
    "    angle_x_loc[a] = 2 * i + 1\n",
    "radius_y_loc = dict()\n",
    "for i, r in enumerate(radii):\n",
    "    radius_y_loc[r] = 2 * i\n",
    "    \n",
    "\n",
    "# make up event responses that scatter on the grid\n",
    "coords = np.c_[channel_map.to_mat()]\n",
    "response_duration = int(0.25 * fs)\n",
    "response_snr = {25: 2, 50: 3, 75: 5, 100: 7}\n",
    "maps = {}\n",
    "\n",
    "data = np.random.randn(channels, (trials * len(condition_labels) + 2) * trial_time)\n",
    "# create the response weights\n",
    "for i, coord in enumerate(product(angles, radii)):\n",
    "    a, r = coord\n",
    "    y = radius_y_loc[r]\n",
    "    x = angle_x_loc[a]\n",
    "    center_location = np.array([y, x])\n",
    "    # inverse distance weight (attenuation strength k = 1.5)\n",
    "    k = 1.5\n",
    "    dist = np.linalg.norm(coords - center_location, axis=1)\n",
    "    distance_weight = (1 + dist * k) ** -1\n",
    "    maps[coord] = distance_weight\n",
    "\n",
    "\n",
    "for i, t in enumerate(exp.time_stamps):\n",
    "    a = exp.angle[i]\n",
    "    r = exp.radius[i]\n",
    "    c = exp.contrast[i]\n",
    "    d_weight = maps[(a, r)]\n",
    "    snr = response_snr[c]\n",
    "    response = np.sin(np.linspace(0, np.pi, response_duration))\n",
    "    response = response / np.std(response) * d_weight[:, None]\n",
    "    data[:, t:t + response_duration] += (snr - 1) * response    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f, axs = plt.subplots(len(radii), len(angles), figsize=(10, 10))\n",
    "for i, j in product(range(len(radii)), range(len(angles))):\n",
    "    m = maps[(angles[j], radii[i])]\n",
    "    im = axs[i, j].imshow(channel_map.embed(m), origin='upper')\n",
    "    axs[i, j].axis('off')\n",
    "    axs[i, j].set_title('Ang {} Rad {}'.format(angles[j], radii[i]))\n",
    "_ = f.suptitle('Localized response weights', fontsize=16)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset = Bunch(name='Dummy dataset',\n",
    "                chan_map=channel_map,\n",
    "                data=data,\n",
    "                Fs=fs,\n",
    "                exp=exp,\n",
    "                units='uV')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "labels = dict()\n",
    "labels['angle'] = ['{:d} deg'.format(int(a)) for a in angles]\n",
    "labels['radius'] = ['{:d} deg'.format(int(r)) for r in radii]\n",
    "labels['contrast'] = ['{:d}%'.format(int(c)) for c in contrasts]\n",
    "\n",
    "colors = dict()\n",
    "colors['angle'] = 'rainbow'\n",
    "colors['contrast'] = 'viridis'\n",
    "colors['radius'] = 'rainbow'\n",
    "\n",
    "ana = Analysis(dataset, \n",
    "               false_epoch_offset=-0.2, \n",
    "               epoch_end=0.4,\n",
    "               peak_min=0,\n",
    "               peak_max=0.2,\n",
    "               scoring='ptp',\n",
    "               condition_labels=labels,\n",
    "               condition_colors=colors,\n",
    "               channel_colors='Blues',\n",
    "               modules=[ChannelAnalysis, AverageAnalysis, ArrayMaps])\n",
    "ana._attach_ep_plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, the basic array map defaults to the primary condition label (\"angle\" here)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana._attach_array_plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tuning curves: three labels\n",
    "\n",
    "With three labels, I need to set which condition is repeated in the tuning curve plot. Every other condition is fixed at its current value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.selected_site = 30"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "channel_ana = get_module(ana, ChannelAnalysis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tuning over angles, repeated for contrast"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "channel_ana.set_value('contrast', 75)\n",
    "f = channel_ana.plot_tuning_curve('angle', repeat_for='radius')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tuning over radii, repeating for contrast"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "channel_ana.set_value('angle', 90)\n",
    "f = channel_ana.plot_tuning_curve('radius', repeat_for='contrast')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The other plots work as before, using a single floating condition, and fixing the others."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "channel_ana.trials_as_heatmaps = True\n",
    "f = channel_ana.channel_plot(floating_condition='radius')\n",
    "f = channel_ana.channel_plot(floating_condition='angle')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Array maps with three labels\n",
    "\n",
    "There is nothing particularly special about the ArrayMaps module. Only a single condition is floating at a time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "array_maps = get_module(ana, ArrayMaps)\n",
    "array_maps.set_value('radius', 12.5)\n",
    "array_maps.set_value('angle', 30)\n",
    "array_maps.set_value('contrast', 100)\n",
    "f = array_maps.plot_maps('radius')\n",
    "f = array_maps.plot_maps('angle')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "r = array_maps.plot_esnr()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
