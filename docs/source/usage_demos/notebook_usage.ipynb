{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Using the Analysis object in notebooks\n",
    "\n",
    "The **Analysis** class (and **TonotopicAnalysis** subclass) are all about evoked potential analysis. These tools organize multi-channel timeseries recordings using a **StimulatedExperiment** object, which is primarily a set of timestamps which have or more condition labels (e.g. tone frequency and amplitude for tonotopy mapping recordings).\n",
    "\n",
    "The Analysis class was designed for hybrid GUI/script usage. This demo shows how an Analysis object and special purpose modulles can be used in a notebook for visualization and trial epoch extraction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ecogdata.util import Bunch, equalize_groups\n",
    "from ecogdata.channel_map import ChannelMap\n",
    "from ecogdata.expconfig.exp_descr import StimulatedExperiment\n",
    "from ecogtopus.anatool import Analysis\n",
    "from ecogtopus.anatool.modules import ChannelAnalysis, AverageAnalysis, ArrayMaps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Faking a dataset\n",
    "\n",
    "I will demonstrate the Analysis object with a synthesized recording and stimulation events for a condition generically named \"event_type\". These are the items expected in a barebones dataset Bunch:\n",
    "\n",
    "* array data (channels x time `numpy.ndarray`)\n",
    "* channel map (`ecogdata.channel_map.ChannelMap`)\n",
    "* experiment event & conditions table (`ecogdata.expconfig.exp_descr.StimulatedExperiment`)\n",
    "* additionally: a name, units, and the sampling frequency"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 7 row 11 column electrode array\n",
    "channel_map = ChannelMap(range(7 * 11), (7, 11))\n",
    "channels = 7 * 11\n",
    "# make 5 * 25 \"trials\" with 0.5 seconds on/off at 200 samps/sec\n",
    "fs = 200\n",
    "trials = 25\n",
    "conditions = 5\n",
    "trial_time = 1 * fs\n",
    "data = np.random.randn(channels, (trials * conditions + 2) * trial_time)\n",
    "# A timestamp each second offset by 1.25 seconds\n",
    "timestamps = np.arange(1, trials * conditions + 1) * fs\n",
    "exp = StimulatedExperiment(time_stamps=timestamps, \n",
    "                           event_tables=dict(event_type=np.array(list(range(conditions)) * trials)),\n",
    "                           condition_order=('event_type',))\n",
    "\n",
    "# make up event responses that march across the channels left to right with distance-weighted strength\n",
    "coords = np.c_[channel_map.to_mat()]\n",
    "response_duration = int(0.25 * fs)\n",
    "response_snr = 5\n",
    "maps = {}\n",
    "\n",
    "for t, c in zip(exp.time_stamps, exp.event_type):\n",
    "    # centered on 3rd row, and a leftward moving column\n",
    "    center_location = np.array([3, (c + 0.5) * 2])\n",
    "    # inverse distance weight (attenuation strength k = 1.5)\n",
    "    k = 1.5\n",
    "    dist = np.linalg.norm(coords - center_location, axis=1)\n",
    "    distance_weight = (1 + dist * k) ** -1\n",
    "    if c not in maps:\n",
    "        maps[c] = distance_weight\n",
    "    # response = np.random.randn(channels, response_duration) * distance_weight[:, None]\n",
    "    response = np.sin(np.linspace(0, np.pi, response_duration))\n",
    "    response = response / np.std(response) * distance_weight[:, None]\n",
    "    data[:, t:t + response_duration] += (response_snr - 1) * response\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f, axs = plt.subplots(1, conditions, figsize=(10, 3))\n",
    "for i in range(conditions):\n",
    "    im = axs[i].imshow(channel_map.embed(maps[i]), origin='upper')\n",
    "    axs[i].axis('off')\n",
    "_ = f.suptitle('Localized response weights', fontsize=16)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset = Bunch(name='Dummy dataset',\n",
    "                chan_map=channel_map,\n",
    "                data=data,\n",
    "                Fs=fs,\n",
    "                exp=exp,\n",
    "                units='uV')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interacting with the main object"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Construct the Analysis object with a dataset and special purpose modules (from `ecogtopus.anatool.modules`, and some other specialized types in `ecogtopus.anatool.tonotopy`). Most keyword arguments are \"Traits\" that can be set at construction and toggled later. For the purpose of notebook usage, note that Traits only take on restricted values, and changing values may trigger other actions (\"callbacks\")."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana = Analysis(dataset, \n",
    "               false_epoch_offset=-0.2, \n",
    "               scoring='rms', \n",
    "               modules=[ChannelAnalysis, AverageAnalysis, ArrayMaps])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Manually draw the average evoked potential plot which is normally triggered when the GUI is created. This plot shows averaged EPs for the presently defined window. It it semi-interactive in notebook mode"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana._attach_ep_plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With no site selected, it shows the grand average on each channel."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.ep_plot.fig"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Extend the plot window length (and trigger the plot update)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.trait_set(epoch_end=0.25)\n",
    "ana._change_ep_plot()\n",
    "ana.ep_plot.fig"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With a given site selected, the traces change to the average per condition. The colors correspond to the Analysis objects colormap for this condition label (\"rainbow\" is the default for all conditions)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.selected_site = 39\n",
    "ana.ep_plot.fig"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Response \"scoring\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The response score is a positive vector-to-scalar function, such as RMS voltage or peak-to-peak voltage. The function computes over the response window `(peak_min, peak_max)`, which is a subset of the visualization window `(epoch_start, epoch_end)`. For the purpose of drawing baseline voltages, both kinds of windows are drawn per timestamp, but offset by the `false_epoch_offset`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.scoring, ana.false_epoch_offset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set scoring for 0 < t < 150 ms. Baseline scores would be made -200 < t < -50 ms, due to the -200 ms offset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.peak_min = 0\n",
    "ana.peak_max = 0.15"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Scoring is always per channel and can be done in three ways:\n",
    "\n",
    "1. Score trial-averaged response --> channels x conditions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.ep_score().shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. Average of single-trial scores --> channels x conditions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.ep_score(single_trials=True, avg_single_trials=True).shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3. All single trial scores. This can either be grouped into conditions x trials (NaN filled, if unbalanced trials) or left in the original trial ordered determined by the experiment timestamps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(ana.ep_score(single_trials=True, avg_single_trials=False, reshape=True).shape)\n",
    "print(ana.ep_score(single_trials=True, avg_single_trials=False, reshape=False).shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To work more directly with the raw voltage traces, use `Analysis.epochs`. This is described in detail in a later section."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_module(ana, cls):\n",
    "    return [m for m in ana.modules if isinstance(m, cls)][0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Single channel plotting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "channel_ana = get_module(ana, ChannelAnalysis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Choose a single channel for plotting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.selected_site = channel_map.lookup(3, 5)\n",
    "ana.ep_plot.fig"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the single-trial score tuning curve for the current scoring mode. This is the mean +/- sem for the trial scores per condition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.selected_site = channel_map.lookup(3, 5)\n",
    "f = channel_ana.plot_tuning_curve()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Mahalanobis distance score\n",
    "ana.scoring = 'mahal'\n",
    "f = channel_ana.plot_tuning_curve()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Peak-to-peak voltage score\n",
    "ana.scoring = 'ptp'\n",
    "f = channel_ana.plot_tuning_curve()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot all traces stacked per condition level (using the example condition name \"event_type\")."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.epoch_start = -0.1\n",
    "ana.epoch_end = 0.4\n",
    "f = channel_ana.channel_plot('event_type')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Can stack raster-style heatmaps instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "channel_ana.trials_as_heatmaps = True\n",
    "f = channel_ana.channel_plot('event_type')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Array-wide score maps"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "array_maps = [m for m in ana.modules if isinstance(m, ArrayMaps)][0]\n",
    "ana.scoring = 'ptp'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Can plot response maps per condition. This is the average of per-trial scores (now peak-peak scoring). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = array_maps.plot_maps('event_type')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again the deafult condition-color map is rainbow. To change this, create an Analysis object "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana = Analysis(dataset, \n",
    "               condition_colors=dict(event_type='viridis'),\n",
    "               false_epoch_offset=-0.2, \n",
    "               scoring='ptp', \n",
    "               modules=[ChannelAnalysis, AverageAnalysis, ArrayMaps],\n",
    "               epoch_end=0.4,\n",
    "               peak_min=0.0,\n",
    "               peak_max=0.15\n",
    "              )\n",
    "array_maps = [m for m in ana.modules if isinstance(m, ArrayMaps)][0]\n",
    "f = array_maps.plot_maps('event_type')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To plot the score map of trial-averaged responses, set `single_trial=False`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "array_maps.single_trial = False\n",
    "f = array_maps.plot_maps('event_type')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot a \"blended\" image that merges the separate maps from above. This chooses, at each pixel, the darkest (minimum) of each RGB value across all the maps. This probably only works well for a few separate condition \"channels\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = array_maps.plot_blended('event_type')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Evoked SNR (ESNR) plot is bias-corrected by default. **Since ESNR involves scoring baseline vectors, it is important to make sure the baseline window does not overlap with responses.** ESNR is caculated per channel using the best average condition score."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = array_maps.plot_esnr()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To see the effect of not correcting bias, show the baseline over baseline ESNR alongside. This should be 0 dB on average. The reason baseline over baseline has positive dB is because the procedure chooses the largest average score. When there is no actual difference in underlying average, the maximum only reflects estimator noise (the standard error of the mean). Without correction:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = array_maps.plot_esnr(correct_bias=False, with_baseline=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The bias is corrected by using the largest average baseline score, after the baseline trials are grouped like response trials. Bias correction is rather conservative, but it ensures that the baseline over baseline ESNR is zero on average. With correction:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = array_maps.plot_esnr(correct_bias=True, with_baseline=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Array wide average series"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avg_ana = get_module(ana, AverageAnalysis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Table plot only works for two conditions (e.g. tone frequency by tone amplitude)"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "f = avg_ana.plot_site_table(...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot all average responses for the chosen condition. Choose from the available values--note that the data type given to set_value() needs to match the available values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avg_ana.values('event_type')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avg_ana.set_value('event_type', 1)\n",
    "avg_ana.current_conditions()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = avg_ana.plot_array_avg()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avg_ana.set_value('event_type', 3)\n",
    "avg_ana.current_conditions()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = avg_ana.plot_array_avg()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Change the error-margin mode: can be one of ('sem', 'stdev', 'pctile', 'plot all')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avg_ana.y_err = 'stdev'\n",
    "f = avg_ana.plot_array_avg()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avg_ana.y_err = 'plot all'\n",
    "f = avg_ana.plot_array_avg()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Access to trial epochs: `Analysis.epochs()`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use the epochs() method to extract and organize trial-by-trial response or baseline. The timeseries returned depend on these values:\n",
    "\n",
    "* epoch_start: window start time in seconds, relative to the timestamp\n",
    "* epoch_end: window end time in seconds, relative to the timestamp (for baseline epochs)\n",
    "* false_epoch_offset: false timestamp offset in seconds\n",
    "* peak_min, peak_max: window used for response scoring\n",
    "\n",
    "*The separate start/end window times are intended for visualization versus analysis purposes.*\n",
    "\n",
    "These attributes are examples of Traits, which can be assigned values in two ways:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.false_epoch_offset = -0.3  # false timestamps start 300 ms before real timestamps\n",
    "ana.epoch_start = -0.1  # full epoch window (e.g. for viz) starts 100 ms before timestamp\n",
    "ana.epoch_end = 0.5  # full epoch window ends 500 ms after timestamp\n",
    "\n",
    "# equivalently\n",
    "ana.trait_set(false_epoch_offset=-0.4, epoch_start=-0.1, epoch_end=0.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Epochs can be gathered in flat order (however the timestamps are ordered in `ana.exp`), or can be grouped per condition. Here is no arrangement:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This is just channels x trials x window-length\n",
    "ana.epochs(arranged=False).shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Arranging the full set of trials permutes the trial dimension and also returns a vector with the trial counts per condition. Here, the first 25 trials go to the first condition level, then 25 to the second level, etc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This re-orders the trials\n",
    "resp, group_sizes = ana.epochs(arranged=True)\n",
    "print(resp.shape, group_sizes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I have simulated balanced trials (25 per condition), which can be simply reshaped to (channels, conditions, repeats, length). In general, use `ecogdata.util.equalize_groups` to split the trial axis into conditions x repeats. For unbalanced trial numbers, some condition groups will have NaN-filled trials. Those data are safely ignored in plotting and in methods like `nanmean`, `nanstd`, etc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# example of unbalanced trials\n",
    "uneven_groups = [25, 25, 25, 25, 20]\n",
    "nan_filled_resp = equalize_groups(resp[:, :120], uneven_groups, axis=1, reshape=True)\n",
    "print(nan_filled_resp.shape)\n",
    "# First 5 points of the last good trial and the first filler trial on channel 0, condition 4\n",
    "nan_filled_resp[0, 4, 19:21, :5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These filler trials are skipped in plotting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "channel = ana.dataset.chan_map.lookup(3, 9)\n",
    "plt.figure()\n",
    "colors = ana._colors['event_type'](np.linspace(0, 1, conditions))\n",
    "lines = []\n",
    "for c in range(conditions):\n",
    "    lc = plt.plot(ana.timebase(), nan_filled_resp[channel, c].T, color=colors[c])\n",
    "    lines.append(lc[0])\n",
    "_ = plt.legend(lines, ['Condition {}'.format(c) for c in range(conditions)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Baseline data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Baseline trials can be returned with `baseline=True`. They can also be arranged, if needed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "baseline = ana.epochs(baseline=True)\n",
    "baseline.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Be careful with baseline settings!** I have inadvertently set the baseline visualization window incorrectly: shifting the timestamp by -400 ms yields -500 - +100 ms, which overlaps with responses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "_ = ax.plot(ana.timebase(), baseline[channel, :25].T, color='gray', lw=0.25)\n",
    "_ = ax.plot(ana.timebase(), nan_filled_resp[channel, 4].T, color='red', lw=0.25)\n",
    "_ = ax.legend(ax.lines[::25], ('baseline', 'response'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ana.false_epoch_offset = -0.65\n",
    "baseline = ana.epochs(baseline=True)\n",
    "fig, ax = plt.subplots()\n",
    "_ = ax.plot(ana.timebase(), baseline[channel, :25].T, color='gray', lw=0.25)\n",
    "_ = ax.plot(ana.timebase(), nan_filled_resp[channel, 4].T, color='red', lw=0.25)\n",
    "_ = ax.legend(ax.lines[::25], ('baseline', 'response'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Other options\n",
    "\n",
    "1. `Analysis.epochs` can return only the scoring sub-window (and equivalent baseline window) using `subresponse=True`.\n",
    "1. Outlier responses can be masked (based on RMS) using `mask_outliers=<IQR-threshold>`. If `ana.epoch_outlier_threshold` is set, then this value is used when `mask_outliers=True`. Returns a masked array.\n",
    "1. A subset of trials can be returned using the `epochs_mask` option.\n",
    "1. When the experimental conditions have multiple labels (e.g. frequencies and amplitudes), there are further options for the `arrange` argument. That is not covered in this simple demo."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
