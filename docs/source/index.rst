.. |date| date::
.. |time| date:: %H:%M

ecogtopus: exploratory analysis for trial-based ecog recordings
===============================================================

This library has many tentacles of shifting hue and unusual cunning.
The central tool is the :py:class:`Analysis<ecogtopus.anatool.base.Analysis>` object with special purpose modules in :py:mod:`ecogtopus.anatool.modules`.

Installation
============

For now, follow the steps

1. http://gabilan2.egr.duke.edu/labdocs/setup.html
2. http://gabilan2.egr.duke.edu/labdocs/ecog_libraries.html#installation-pip-editable-mode

When installed on your own machine, command-line entry points for the GUI-based tools are installed as

* ``analysis_tool.py`` -- generic experiment analysis
* ``tonotopic_analysis.py`` -- specialized tonotopy tool

For remote use, see the Usage Demos below (work in progress).

What's included
===============

These tools can be used for the following visualizations, and several others

* tuning curves: :py:class:`ChannelAnalysis<ecogtopus.anatool.modules.ChannelAnalysis>`
* array activation maps: :py:class:`ArrayMaps<ecogtopus.anatool.modules.ArrayMaps>`
* ESNR maps: :py:class:`ArrayMaps<ecogtopus.anatool.modules.ArrayMaps>`
* trial-ordered and trial-averaged responses:

  - timeseries: :py:class:`ChannelAnalysis<ecogtopus.anatool.modules.ChannelAnalysis>` and :py:class:`AverageAnalysis<ecogtopus.anatool.modules.AverageAnalysis>`
  - power spectra: :py:class:`FrequencyAnalysis<ecogtopus.anatool.modules.FrequencyAnalysis>`
  - spectrograms: :py:class:`Spectrograms<ecogtopus.anatool.modules.Spectrograms>`

The library was conceived to be simultaneously "scriptable" and interactive through a GUI.
See the usage demonstrations for usage through notebooks or other interactive interpreters.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   usage_demos/index
   tono_ana_tool
   api

Misc
====

Documents rebuilt |date| at |time|.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
