# ecogtopus

A multi-armed repository for trial-based analysis from micro-ecog recordings. This package is a new Python-3 home for

* Analysis GUI tool (formerly `ecogana.anacode.anatool.base.Analysis`) 
* decoding package (formerly `ecogana.anacode.decoding`)
* general evoked-response scoring and visualization

## Other ECoG Python code
ecogtopus depends on [ecogdata](https://github.com/miketrumpis/ecogdata) and [ecoglib](https://bitbucket.org/tneuro/ecoglib/src/master/).

## Other notable Python packages

* traits & traitsui for lazy UI design
* scikit-learn for classification algorithms
* numpy & scipy for general numerix and stats

## Basic example
```python
from ecogdata.devices.data_util import load_experiment_auto
from ecogtopus.anatool import Analysis
dataset = load_experiment_auto('demo/testA', bandpass=(1, 200))
# set epoch window to 150 ms with a 25 ms pre-stim lead
# set scoring window from 5-40 ms
ana = Analysis(dataset, epoch_start=-0.025, epoch_end=0.125, peak_min=0.05, peak_max=0.04)
trials = ana.get_epochs()
ana.scoring = 'mahal'
ep_scores = ana.score()
```

## Analysis tool demo: tone-evoked potentials

These are example windows from the ``tonotopic_analysis.py`` tool.

### Main GUI

![main tool](docs/images/tono_ana.png)

### Single channel trial raster

![main tool](docs/images/tono_ana2.png)

### Evoked SNR map and boxplot

![main tool](docs/images/tono_ana3.png)

### Stimulus (tones) response map

![main tool](docs/images/tono_ana4.png)

